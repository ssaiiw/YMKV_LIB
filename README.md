# YMKV_LIB

#### 介绍
这是一个采用C语言编写的图像处理与机器视觉库，可以运行在PC，STM32或其他嵌入式平台上，其中包含了常用的许多常用的图像处理算法。

This is an image processing and machine vision library written in C language, can run on PC, STM32 or other embedded platform, which contains many commonly used image processing algorithms.

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  NOOTHER文件夹下为不带ZBAR和QRENCODE库的YMKV工程，不支持二维码识别和生成二维码
2.  HAOTHER文件夹下为带ZBAR和QRENCODE库的YMKV工程，支持二维码识别和生成二维码
3.  下载完工程编译时需注意：
-------工程属性->链接器->常规->附加库目录修改为你自己的路径，比如  E:\xxx\YMKV_LIB\NOOTHER\YMKV\YAOMI;
-------工程属性->链接器->输入->附加依赖项加上YMKV.lib（如果上面没有的话）
4. 待处理图片缓冲在pic.h文件夹数组
5. 待处理图片大小通过camera.h文件中的 pic_h，pic_w  两个参数进行设置
6. 官方支持QQ群：妖米视觉（774265551）
7. 教程官方B站妖米猫人发布地址：[https://space.bilibili.com/314022084]

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
