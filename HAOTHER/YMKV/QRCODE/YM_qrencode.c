
#include"Puse.h"
#include"YMType.h"
#include"YMKV.h"
#include"YM_code.h"
//二维码版本  || 符号规格
/*
	版本1为21*21，此后每上升一个版本，边长+4 ，直到版本40为117*117
	版本为>1时，会有矫正图形，否则没有。
	定位图形2个，用于对符号信息密度和版本的确定，以及模块坐标参考
	位置探测图形和分隔符各三个
*/
ImgType ymqre_version_init(u8 qr_version)
{
	u16 code_size = 4 * qr_version + 17;
	u16 i, j,ic,jc;
	ImgType mycode;
	u8* datap;
	u8 bitb;
	u16 qrjlen, qrjnum, jlen,relnum;
	//位置探测图形
	u8 posdp[7][7] = {     1,1, 1,1,1, 1,1,
						   1,0, 0,0,0, 0,1,
						   1,0, 1,1,1, 0,1,
						   1,0, 1,1,1, 0,1,
						   1,0, 1,1,1, 0,1,
						   1,0, 0,0,0, 0,1,
						   1,1, 1,1,1, 1,1,
	};
	//定位图形
	u8 jposp[5][5] = { 1,1, 1, 1,1,
					   1,0, 0, 0,1,
					   1,0, 1, 0,1,
					   1,0, 0, 0,1,
					   1,1, 1, 1,1,
	};
	if (qr_version > 40)
		return Null;

	//申请二维码内存空间
	mycode = Creat.img(code_size, code_size, imgGray); //二维码图像
	datap = mycode->data;
	for (i = 0; i < code_size; i++)
		for (j = 0; j < code_size; j++)
		{
			datap[i*code_size + j] = 125; //初始化未使用标志
		}
	//初始化三个位置探测图形
	for (i = 0; i < 7; i++)
		for (j = 0; j < 7; j++)
		{
			datap[i*code_size + j] = posdp[i][j];
		}
	for (i = 0; i < 7; i++)
		for (j = 0; j < 7; j++)
		{
			jc = code_size - 7 + j;
			datap[i*code_size + jc] = posdp[i][j];
		}
	for (i = 0; i < 7; i++)
		for (j = 0; j < 7; j++)
		{
			ic = code_size - 7 + i;
			datap[ic*code_size + j] = posdp[i][j];
		}
	//初始化三个探测分隔符
	for (i = 0; i < 8; i++)
	{
		j = 7;
		datap[i*code_size + j] = 0;
		j = code_size - 8;
		datap[i*code_size + j] = 0;
	}
	for (j = 0; j < 8; j++)
	{
		i = 7;
		datap[i*code_size + j] = 0;
		i = code_size - 8;
		datap[i*code_size + j] = 0;
	}
	for (j = code_size - 8; j < code_size; j++)
	{
		i = 7;
		datap[i*code_size + j] = 0;
	}
	for (i = code_size - 8; i < code_size; i++)
	{
		j = 7;
		datap[i*code_size + j] = 0;
	}
	//初始化两个定位图形
	bitb = 1;
	for (i = 8; i < code_size - 8; i++)
	{
		j = 6;
		datap[i*code_size + j] = bitb;
		bitb = (bitb == 0) ? 1 : 0;
	}
	bitb = 1;
	for (j = 8; j < code_size - 8; j++)
	{
		i = 6;
		datap[i*code_size + j] = bitb;
		bitb = (bitb == 0) ? 1 : 0;
	}
	//初始化矫正图形 根据版本号确定数量 从2版本往上均设置有矫正图形 具体位置参考GB/T 18284-2000
	if (qr_version > 1)
	{
		//版本每上升7时，每边矫正图形数+1
		code_size=4 * qr_version + 17;
		//版本每上升7时，每边矫正图形数+1
		qrjlen = code_size - (2 * 7);
		qrjnum = qrjlen / (4 * 7) + 1; //矫正图形数

		qrjlen += 1;
		//首矫正图形间距
		jlen = qrjlen / qrjnum;

		if ((qrjnum*jlen) != qrjlen) //非整除时
		{
			jlen = (u16)ceil(qrjlen / (double)qrjnum);//向上取整
		}
		if ((jlen % 2) != 0)
		{
			jlen++;
		}

		//获得矫正位置坐标
		u8* jxyp = graph.mem->CCMmalloc(qrjnum+1);
		for (i = 0; i < qrjnum; i++)
		{
			jxyp[i + 1] = jlen;
		}
		jxyp[1] = qrjlen - (qrjnum - 1)*jlen;

		jxyp[0] = 6;
		for (i = 1; i <=qrjnum; i++)
		{
			jxyp[i]+= jxyp[i-1];
		}
		//画出矫正图形
		for (ic = 0; ic <= qrjnum; ic++)
			for (jc = 0; jc <= qrjnum; jc++)
			{
				//画出非探测图形上的矫正图形
				if (((ic == 0) && (jc == 0)) || ((ic == qrjnum) && (jc == 0)) || ((ic == 0) && (jc == qrjnum)))
					continue;
				for (i = 0; i <5; i++)
				{
					for (j = 0; j <5; j++)
					{
						datap[(jxyp[ic]+i-2)*code_size + (jxyp[jc]+j-2)] = jposp[i][j];
					}
				}
					
			}
		graph.mem->CCMfree(jxyp);
		//剩余位
		if (qrjnum == 1)
			relnum = 7;
		else if (qrjnum == 2)
			relnum = 0;
		else if (qrjnum == 3)
			relnum = 3;
		else if (qrjnum == 4)
			relnum = 4;
		else if (qrjnum == 5)
			relnum = 3;
		else if (qrjnum == 5)
			relnum = 0;
	}
	else 
		relnum = 0;
	
	//初始化剩余位
	
	return mycode;
}

//二维码生成模式  || 字符集
/*
	模式是将特定字符串表示成bit串的方式，下列模式是根据缺省的ECI相关的字符值及任务确定的。
	如果采用其他ECI，最佳压缩模式就是使用字节值，而不是具体的字符任务。
*/
void ymqre_str_mode(char* str,u8 mode)
{
	if (str == Null)
		return;

	//生成模式判断
	switch (mode)
	{
	case QR_NUM_MODE: //数字类型：对十进制数字0-9（ASCII值：30H-39H）编码，通常密度为每10位表示3个数字

		break;

	case QR_AN_MODE: //字母数字类型：数字0-9（ASCII值：30H-39H）,字母A-Z（ASCII值：41H-5AH），
					//              9个符号：SP,$,%,*,+,-,.,/,（ASCII值：20H,24H,25H,2AH,2BH,2DH,2EH,3AH）
					//  通常密度为每11位表示2个字符

		break;

	case QR_HZ_MODE://汉字字符类型（中国）：用于表示GB2312规定的双字节表示的的汉字和非汉字字符，其值为GB2312内码值
					//通常用13位来表示每个双字节字符

		break;

	case QR_8B_MODE://8位字节类型：（ASCII值：00H-FFH），用8位来表示每个字符对应的ASCII值

		break;
	case QR_ECI_MODE://扩充模式：其他的，比如用户自定义等等

		break;

	default:
		break;
	}
}

//数据类型及最大容量
/*

*/
void myqr_mode()
{
	//数字类型：数据7089个字符
//QR_NUM_MODE:

	//字母数字类型：数据4296个字符

	//8位字节类型：数据2953个字符

	//汉字字符类型（中国）：数据1817个字符

}

/*
数据·表示 ： 黑为1 白为0
*/

//纠错级别
/*
	L级：约纠正7%的数据码字错误
	M级：约纠正15%的数据码字错误
	Q级：约纠正25%的数据码字错误
	H级：约纠正30%的数据码字错误
*/

//掩模
/*
	使符号中黑色与白色色块比例接近1:1，使因相邻模块排列造成是译码困难性降为最小
*/