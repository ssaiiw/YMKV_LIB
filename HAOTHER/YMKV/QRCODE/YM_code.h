#pragma once

#define	QR_NUM_MODE  1   //数字模式
#define	QR_AN_MODE   2   //字母数字模式
#define	QR_HZ_MODE   3   //汉字模式
#define	QR_8B_MODE   4   //8位字节模式
#define	QR_ECI_MODE  5   //ECI模式
