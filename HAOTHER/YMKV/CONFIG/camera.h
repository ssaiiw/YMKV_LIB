#ifndef _CAM_H
#define _CAM_H
#include "Puse.h"

//********************************************************************************	 
//本例程只供学习使用，未经许可，不得用于其它任何用途
//YMKV开发板
//摄像头接口配置代码   
//项目：妖米视觉
//创建日期:2019/11/5
//版本：V1.0
//版权所有，盗版必究。
//沈阳妖米科技责任有限公司
//********************************************************************************
#define pic_h 160
#define pic_w 200
//#define pic_h 120
//#define pic_w 160

#define picbuf_len pic_h*pic_w   			//定义JPEG数据缓存jpeg_buf的大小(*4字节)
extern volatile u32 pic_data_len; 			//buf中的JPEG有效数据长度 



////JPEG尺寸支持列表
extern const u16 jpeg_size[][2];
enum picsize { QQVGA = 0, QVGA, VGA, SVGA, XGA };
void CameraInit(void);


void continue_pic(void);

#endif

