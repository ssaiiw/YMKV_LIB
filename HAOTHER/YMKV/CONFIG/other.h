#ifndef _other_h
#define _other_h

#include "YMKV.h"

extern u16 YMKV_graytorgb565(u8 gray);
extern ImgType CAM;
void draw_blobs(u16 startx,u16 starty,BlobType pthis);
void IMG_Init(void);
void Temp_Init(ImgType temp);
void qshow(int startx,int starty ,int deta);
void showfeature(u16 startx,u16 starty,FeatureList myfeature);
void Histshow(ImgType gray,int startx,int starty ,int width);
void drawline_rou_theta(u16 startx,u16 starty,ImgType myimg,SVectordType rou_theta);
void drawline_hough(u16 startx,u16 starty,LinesType mylines);
void qrlist_show(u16 startx,u16 starty,QRListType pthis);

#endif
