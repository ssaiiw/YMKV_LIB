#include"YMCreat.h"
#include"graph.h"
#include"YMKV.h"
#include"stdarg.h"


#include "stdlib.h"
#include "math.h"


extern float YMKV_Averkernel(int x,int y);
extern float YMKV_Gausskernel(float Gausssgm,int x,int y);
extern float YMKV_LoGkernel(float Gausssgm,int x,int y);

// a List
void YMKV_Creat_mylist_init(void *pthis,u16 w,u16 h)
{
    ListType mylis=(ListType)pthis;

    mylis->wp=w;
    mylis->hp=h;
}
ListType YMKV_Creat_mylist_free(void *pthis)
{
    ListType mylis=(ListType)pthis;
    ListType last;

    last=mylis->lastlist;
    if(last == Null)//栈顶
        return mylis;//返回栈顶地址
	graph.mem->CCMfree(mylis);
	return last;
}
ListType YMKV_Creat_alist_creat(ListType last)
{
    ListType mylis;

    mylis=(ListType)graph.mem->CCMmalloc(sizeof(listype));

    mylis->init=YMKV_Creat_mylist_init;
    mylis->free=YMKV_Creat_mylist_free;
    //挂载上一个
    if(last!=Null)
			mylis->lastlist=last;
    return mylis;
}
//  List s
ListType* YMKV_Creat_liststrmalloc(int num)
{
    ListType *p;
    int i;
    p=(ListType *)graph.mem->SRAM0malloc(sizeof(ListType)*num);//申请数组 p[num]
    for(i=0;i<num;i++)
    {
        p[i]=Creat.alist(Null);
        p[i]->init(p[i],0,0);
    }
    return p;
}
void YMKV_Creat_liststrfree(ListType* pthis,int num)
{
    int i;
    for(i=0;i<num;i++)
    {
        do
        {
            pthis[i]=pthis[i]->free(pthis[i]);
        }
        while(pthis[i]->lastlist!=Null);
        graph.mem->SRAM0free(pthis[i]);
    }
    graph.mem->SRAM0free(pthis);
}

// a Line
void YMKV_Creat_myline_init(void *pthis,u16*xy)
{
    LinesType mylis=(LinesType)pthis;

    mylis->xy[0]=xy[1];
    mylis->xy[1]=xy[0];
    mylis->xy[2]=xy[3];
    mylis->xy[3]=xy[2];
}
LinesType YMKV_Creat_myline_free(void *pthis)
{
    LinesType mylis=(LinesType)pthis;
    LinesType last;
    
    last=mylis->lastline;
    if(last == Null)//栈顶
        return mylis;//返回栈顶地址
	graph.mem->CCMfree(mylis->xy);
	graph.mem->CCMfree(mylis);
	return last;
}
//  Line s
LinesType YMKV_Creat_aline_creat(LinesType last)
{
    LinesType mylis;

    mylis=(LinesType)graph.mem->CCMmalloc(sizeof(lintype));
	  mylis->xy=(u16*)graph.mem->CCMmalloc(sizeof(u16)*4);
    mylis->init=YMKV_Creat_myline_init;
    mylis->free=YMKV_Creat_myline_free;
    //挂载上一个
    mylis->lastline=last;
	
    return mylis;
}
void YMKV_Creat_linestrfree(LinesType pthis)
{
	if(pthis==Null)return;
   while(pthis->lastline!=Null)
		 pthis=pthis->free(pthis);
	 pthis->free(pthis);
}

//	image data
ImgType YMKV_Creat_img_creat(u16 width,u16 height,ImgUn Imgform)
{
  ImgType myimg;

  myimg=(ImgType)graph.mem->SRAM0malloc(sizeof(imgtype));//图像头信息
	myimg->width=width;
	myimg->height=height;
	myimg->sizet=Imgform;

	myimg->malloc=YMKV_Creat_imgmalloc;
	myimg->free=YMKV_Creat_imgfree;
	myimg->data=(u8*)myimg->malloc(myimg);
	return myimg;
}
u8* YMKV_Creat_imgmalloc(void *pthis)
{
    ImgType myimg=(imgtype*)pthis;
    myimg->pmlc=0;

	return (u8*)graph.mem->SRAM1malloc((myimg->height)*(myimg->width)*(myimg->sizet));
}
void YMKV_Creat_imgfree(void *pthis)
{
  ImgType myimg=(imgtype*)pthis;
	if(pthis==Null)return;
	graph.mem->SRAM1free(myimg->data);
	graph.mem->SRAM0free(myimg);
}
ImgType YMKV_Creat_ImgCopyto(ImgType myimg,u16 startw,u16 starth,u16 width,u16 height)
{
    ImgType newimg;
    u16 i,j;
	  u8 *datau8pin,*datau8pout;
	  u16 *datau16pin,*datau16pout;

    newimg=Creat.img(width,height,myimg->sizet);
	  switch(myimg->sizet)
		{
			case imgGray:
				datau8pin=myimg->data;
				datau8pout=newimg->data;
				for(i=0;i<height;i++)
				{
						for(j=0;j<width;j++)
						{
								datau8pout[i*width+j]=datau8pin[(i+starth)*myimg->width+(j+startw)];
						}
				}
				break;
			case imgRGB:
				datau16pin=(u16*)myimg->data;
				datau16pout=(u16*)newimg->data;
				for(i=0;i<height;i++)
				{
						for(j=0;j<width;j++)
						{
								datau16pout[i*width+j]=datau16pin[(i+starth)*myimg->width+(j+startw)];
						}
				}
				break;
		}
    
	return newimg;
}

ImgType YMKV_Creat_Img_Backup(ImgType myimg)
{
    ImgType newimg;
    u16 i,j;
    u16 width,height;
    width=myimg->width;
    height=myimg->height;

    newimg=Creat.img(width,height,myimg->sizet);
    for(i=0;i<height;i++)
    {
        for(j=0;j<width;j++)
        {
            newimg->data[i*width+j]=myimg->data[i*width+j];
        }
    }
	return newimg;
}

ImgType YMKV_Creat_DataToImg(u8* data,u16 width,u16 height,ImgUn Imgform)
{
    ImgType newimg;
	
		newimg=(ImgType)graph.mem->SRAM0malloc(sizeof(imgtype));//图像头信息
		newimg->width=width;
		newimg->height=height;
		newimg->sizet=Imgform;
		newimg->malloc=YMKV_Creat_imgmalloc;
		newimg->free=YMKV_Creat_imgfree;
		newimg->data=data;
	return newimg;
}

// kernel 
KernelType YMKV_Creat_kernel_creat(u8 R_size)
{
    KernelType myker;

    myker=(KernelType)graph.mem->CCMmalloc(sizeof(kerneltype));
		myker->R_size=R_size;
		myker->malloc=YMKV_Creat_kernelmalloc;
		myker->free=YMKV_Creat_kernelfree;
    myker->init=YMKV_Creat_Kernel_init;
    myker->sgm=0;

	return myker;
}
void YMKV_Creat_Kernel_init(void *pthis,u8 what_kernel,...)
{
    int i,j,kw;
    float value,tol;
    u8 r;
    KernelType myker;
    va_list arglist;

    myker=(KernelType)pthis;
    r=myker->R_size;
	  kw=2*r+1;
    myker->data=(float*)myker->malloc(myker);//开辟内存

    

    switch(what_kernel)
    {
    case Aver:
                tol=(float)(kw*kw);
                for(i=-r;i<=r;i++)
                {
                    for(j=-r;j<=r;j++)
                    {
                        value=YMKV_Averkernel(i,j)/tol;
                        myker->data[(i+r)*kw+(j+r)]=value;
                    }
                }
                break;
    case Gauss:
                if(myker->sgm==0)
                {
                    va_start(arglist,what_kernel);
                    myker->sgm=(float)va_arg(arglist,double);
                    va_end(arglist);//结束list
                }
								tol=0.0;
                for(i=-r;i<=r;i++)
                {
                    for(j=-r;j<=r;j++)
                    {
                        value=YMKV_Gausskernel(myker->sgm,i,j);
												tol+=value;
                        myker->data[(i+r)*kw+(j+r)]=value;
                    }
                }
								for(i=0;i<kw;i++)
                {
                    for(j=0;j<kw;j++)
                    {
                        myker->data[i*kw+j]=myker->data[i*kw+j]/tol;
                    }
                }
                break;
    case LOG:
                if(myker->sgm==0)
                {
                    va_start(arglist,what_kernel);
                    myker->sgm=(float)va_arg(arglist,double);
                    va_end(arglist);//结束list
                }
								tol=0.0;
                for(i=-r;i<=r;i++)
                {
                    for(j=-r;j<=r;j++)
                    {
                        value=YMKV_LoGkernel(myker->sgm,i,j);
												tol+=value;
                        myker->data[(i+r)*kw+(j+r)]=value;
                    }
                }
								for(i=0;i<kw;i++)
                {
                    for(j=0;j<kw;j++)
                    {
                        myker->data[i*kw+j]=myker->data[i*kw+j]/tol;
                    }
                }
                break;
		case BilFilter:
								if(myker->sgm==0)
                {
                    va_start(arglist,what_kernel);
                    myker->sgm=(float)va_arg(arglist,double);
                    myker->sgmgray=(float)va_arg(arglist,double);
                    va_end(arglist);//结束list
                }
								tol=0.0;
                for(i=-r;i<=r;i++)
                {
                    for(j=-r;j<=r;j++)
                    {
                        value=YMKV_Gausskernel(myker->sgm,i,j);
												tol+=value;
                        myker->data[(i+r)*kw+(j+r)]=value;
                    }
                }
								for(i=0;i<kw;i++)
                {
                    for(j=0;j<kw;j++)
                    {
                        myker->data[i*kw+j]=myker->data[i*kw+j]/tol;
                    }
                }
                break;
    }
}
float* YMKV_Creat_kernelmalloc(void *pthis)
{
    KernelType mykernel=(KernelType)pthis;
	return (float*)graph.mem->CCMmalloc((2*mykernel->R_size +1)*(2*mykernel->R_size +1)*sizeof(float));
}

void YMKV_Creat_kernelfree(void *pthis)
{
  KernelType mykernel=(KernelType)pthis;
	graph.mem->CCMfree(mykernel->data);
	graph.mem->CCMfree(mykernel);
}

//edge kernel
EdgeKernelType YMKV_Creat_edgeker_creat(u8 type,...)
{
    EdgeKernelType myedgeker;
    va_list arglist;

    myedgeker=(EdgeKernelType)graph.mem->CCMmalloc(sizeof(edgekerneltype));
    myedgeker->type=type;
    myedgeker->free=YMKV_Creat_edgekernelfree;
    if(myedgeker->type==Canny)
    {
        va_start(arglist,type);//头一个参数
        myedgeker->canythremax=(u8)va_arg(arglist,int);//取出后一个参数
        myedgeker->canythremin=(u8)va_arg(arglist,int);//取出后一个参数
        va_end(arglist);//结束list
    }
    return myedgeker;
}
void YMKV_Creat_edgekernelfree(void *pthis)
{
    EdgeKernelType mykernel=(EdgeKernelType)pthis;
	  graph.mem->CCMfree(mykernel);
}


// float vector
FVectordType YMKV_Creat_fVetcor_creat(int sizenum)
{
    FVectordType myvec;

    myvec=(FVectordType)graph.mem->SRAM0malloc(sizeof(dvectortype));

    myvec->len_size=sizenum;
    myvec->malloc=YMKV_Creat_fvectormalloc;
    myvec->free=YMKV_Creat_fvectorfree;

    myvec->data=(float*)myvec->malloc(myvec);

    return myvec;
}
float* YMKV_Creat_fvectormalloc(void *pthis)
{
    FVectordType myvec=(FVectordType)pthis;
	return (float*)graph.mem->SRAM1malloc(myvec->len_size*sizeof(float));
}

void YMKV_Creat_fvectorfree(void *pthis)
{
    FVectordType myvec=(FVectordType)pthis;
	graph.mem->SRAM1free(myvec->data);
	graph.mem->SRAM0free(myvec);
}

//short vector
SVectordType YMKV_Creat_SVetcor_creat(int sizenum)
{
    SVectordType myvec;

    myvec=(SVectordType)graph.mem->SRAM0malloc(sizeof(svectortype));

    myvec->len_size=sizenum;
    myvec->malloc=YMKV_Creat_svectormalloc;
    myvec->free=YMKV_Creat_svectorfree;

    myvec->data=(short*)myvec->malloc(myvec);

    return myvec;
}
short* YMKV_Creat_svectormalloc(void *pthis)
{
    SVectordType myvec=(SVectordType)pthis;
	return (short*)graph.mem->SRAM1malloc(myvec->len_size*sizeof(short));
}
void YMKV_Creat_svectorfree(void *pthis)
{
    SVectordType myvec=(SVectordType)pthis;
	graph.mem->SRAM1free(myvec->data);
	graph.mem->SRAM0free(myvec);
}

//blob
BlobType YMKV_Creat_blob_creat( BlobType lastblob)//挂队列尾
{
    BlobType myblob;

    myblob=(BlobType)graph.mem->SRAM0malloc(sizeof(blobype));

    myblob->free=YMKV_Creat_blobfree;
    myblob->init=YMKV_Creat_blobInit;
    myblob->lastlist=lastblob;
    myblob->nextlist=Null;

    return myblob;
}
void YMKV_Creat_blobInit(void *pthis,u16 pw,u16 ph,u16 width,u16 height,unsigned int area)
{
    BlobType p=(BlobType)pthis;

    p->area=area;
    p->height=height;
    p->width=width;
    p->wp=pw;
    p->hp=ph;
}

BlobType YMKV_Creat_blobfree(void *pthis)
{
    BlobType p=(BlobType)pthis;

    if(p->lastlist!=Null)//非表头
    {
        if(p->nextlist!=Null) //位于中间
        {
            p->lastlist->nextlist=p->nextlist;//
            p->nextlist->lastlist=p->lastlist;
            p=p->nextlist;//返回下一个
        }
        else //表尾
        {
            p->lastlist->nextlist=Null;
            p=p->lastlist;//返回上一个
        }
    }
    else //表头
		{
        p=p->lastlist;
		}

    graph.mem->SRAM0free(pthis);
    return p;
}
//色块销毁
void YMKV_Creat_blobsfree(BlobType pthis)
{
	if(pthis==Null)return;
	while(pthis!=Null)
	{
		pthis=pthis->free(pthis);
	}
}

// filter FFT
FFTFilterType YMKV_Creat_fftfilter_creat(u8 mytype,u16 myD0, u8 gsn)
{
    FFTFilterType myfilt;

    myfilt=(FFTFilterType)graph.mem->CCMmalloc(sizeof(fftfiltertype));

    myfilt->type=mytype;
    myfilt->n=gsn;
    myfilt->D0=myD0;
    myfilt->free=YMKV_Creat_fftfilterfree;

    return myfilt;
}
void YMKV_Creat_fftfilterfree(void *pthis)
{
    FFTFilterType myfilt=(FFTFilterType)pthis;
    graph.mem->CCMfree(myfilt);
}

// filter DWT
DWTFilterType YMKV_Creat_dwtfilter_creat(u8 mytype, u8 grade_num,float minxs)
{
    DWTFilterType myfilt;

    myfilt=(DWTFilterType)graph.mem->CCMmalloc(sizeof(dwtfiltertype));

    myfilt->type=mytype;
    myfilt->num=grade_num;
		myfilt->threshold=minxs;
    myfilt->free=YMKV_Creat_dwtfilterfree;

    return myfilt;
}
void YMKV_Creat_dwtfilterfree(void *pthis)
{
    DWTFilterType myfilt=(DWTFilterType)pthis;
    graph.mem->CCMfree(myfilt);
}

// threshold gray
Gray2ThresholdType YMKV_Creat_gray2threshold_creat(short thre_max,short thre_min)
{
    Gray2ThresholdType gray2thre;

    gray2thre=(Gray2ThresholdType)graph.mem->SRAM0malloc(sizeof(gray2thresholdtype));
    gray2thre->thremax=thre_max;
    gray2thre->thremin=thre_min;
    gray2thre->free=YMKV_Creat_Gray2Threfree;

    return gray2thre;
}
void YMKV_Creat_Gray2Threfree(void *pthis)
{
    Gray2ThresholdType gray2thre=(Gray2ThresholdType)pthis;
    graph.mem->SRAM0free(gray2thre);
}
// threshold color
Color2ThresholdType YMKV_Creat_color2threshold_creat(short threshold[6],u8 type)
{
    Color2ThresholdType color2thre;

    color2thre=(Color2ThresholdType)graph.mem->SRAM0malloc(sizeof(color2thresholdtype));

    color2thre->type=type;
    color2thre->thre1=YMKV_Creat_gray2threshold_creat(threshold[0],threshold[1]);
    color2thre->thre2=YMKV_Creat_gray2threshold_creat(threshold[2],threshold[3]);
    color2thre->thre3=YMKV_Creat_gray2threshold_creat(threshold[4],threshold[5]);

    color2thre->free=YMKV_Creat_Color2Threfree;

    return color2thre;
}

void YMKV_Creat_Color2Threfree(void *pthis)
{
    Color2ThresholdType Color2thre=(Color2ThresholdType)pthis;

    Color2thre->thre1->free(Color2thre->thre1);
    Color2thre->thre2->free(Color2thre->thre2);
    Color2thre->thre3->free(Color2thre->thre3);
    graph.mem->SRAM0free(Color2thre);
}

// Feature kernel
FeatureType YMKV_Creat_feature_creat(u8 feature_point_type,...)
{
    FeatureType myfeature_point=(FeatureType)graph.mem->SRAM0malloc(sizeof(featuretype));
    va_list arglist;

    myfeature_point->type=feature_point_type;
    myfeature_point->free=YMKV_Creat_feature_free;
    if(myfeature_point->type==SUSAN ||myfeature_point->type==Harris)
    {
        va_start(arglist,feature_point_type);//头一个参数
        myfeature_point->Threshold=(u8)va_arg(arglist,int);//取出后一个参数
        va_end(arglist);//结束list
    }
    return myfeature_point;
}

void YMKV_Creat_feature_init(FeatureType pthis,...)
{
    va_list arglist;

    pthis->free=YMKV_Creat_feature_free;
    if(pthis->type==SUSAN)
    {
        va_start(arglist,pthis);//头一个参数
        pthis->type=(u8)va_arg(arglist,int);//取出后一个参数
        pthis->Threshold=(u8)va_arg(arglist,int);//取出下一个参数
        va_end(arglist);//结束list
    }
}
void YMKV_Creat_feature_free(void* pthis)
{
    graph.mem->SRAM0free((FeatureType)pthis);
}

// Feature point
void YMKV_Creat_features_creat(void*pthis, u8 xyc_num,u8 featdis_num)
{
    FeatureList thislis=(FeatureList)pthis;
    Features myfeats;
		
	  if(thislis->thisfeature!=Null) return;
	  myfeats=(Features)graph.mem->SRAM0malloc(sizeof(featurestype));
    myfeats->xycnum=xyc_num;
    myfeats->featdiscrynum=featdis_num;
    myfeats->xyc=(short*)graph.mem->SRAM1malloc(xyc_num*sizeof(short));
    myfeats->featdis=(short*)graph.mem->SRAM1malloc(featdis_num*sizeof(short));
    myfeats->free=YMKV_Creat_feats_free;
    thislis->thisfeature=myfeats;
}
void YMKV_Creat_feats_free(void *pthis)
{
    Features myfeats=(Features)pthis;
    graph.mem->SRAM1free(myfeats->featdis);
    graph.mem->SRAM1free(myfeats->xyc);
    graph.mem->SRAM0free(myfeats);
}
FeatureList YMKV_Creat_Feature_list_creat(FeatureList last)
{
    FeatureList myfeatlis;

    myfeatlis=(FeatureList)graph.mem->SRAM0malloc(sizeof(featurelistype));
		
		if(myfeatlis==Null)return last;
    myfeatlis->thisfeature=Null;
    myfeatlis->featureinit=YMKV_Creat_features_creat;
    myfeatlis->lastlist=last;
    myfeatlis->free=YMKV_Creat_featlis_free;
    //挂载下一个

    return myfeatlis;
}
FeatureList YMKV_Creat_featlis_free(void *pthis)
{
    FeatureList mylis=(FeatureList)pthis;
    FeatureList last;

		if(mylis==Null)return Null;
    last=mylis->lastlist;
  mylis->thisfeature->free( mylis->thisfeature);
	graph.mem->SRAM0free(mylis);
	return last;
}

void YMKV_Creat_featlists_free(FeatureList myfeature)
{
	  FeatureList p=myfeature;
	  if(p==Null)return;
	  while(p!=Null)
    {
        p=p->free(p);
    }
}

// QR List
void YMKV_Creat_qrlist_init(void *pthis,u16 w,u16 h)
{
    QRListType mylis=(QRListType)pthis;

    mylis->wp=w;
    mylis->hp=h;
}
QRListType YMKV_Creat_aqrlist_free(void *pthis)
{
    QRListType mylis=(QRListType)pthis;
    QRListType last = Null;
    if(mylis != Null)
		{
      last=mylis->lastlist;
		}
	return last;
}
QRListType YMKV_Creat_qrlist_creat(QRListType last)
{
    QRListType mylis;

    mylis=(QRListType)graph.mem->CCMmalloc(sizeof(qrlistype));

    mylis->init=YMKV_Creat_qrlist_init;
    mylis->free=YMKV_Creat_aqrlist_free;
    //挂载上一个
		mylis->lastlist=last;
	
    return mylis;
}
void YMKV_Creat_qrlist_free(QRListType pthis)
{
	QRListType p;
    while(pthis)
		{
			p=pthis;
			pthis=p->free(p);
			graph.mem->CCMfree(p);
		}
}
