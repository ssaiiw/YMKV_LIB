
#ifndef lcd_h

#define lcd_h
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;

extern u16 POINT_COLOR;
extern u16 BACK_COLOR;

extern u16 LCD_W, LCD_H;

#include "UWindows.h"

//画笔颜色
#define WHITE         	 0xFFFF
#define BLACK         	 0x0000	  
#define BLUE         	 0x001F  
#define BRED             0XF81F
#define GRED 			 0XFFE0
#define GBLUE			 0X07FF
#define RED           	 0xF800
#define MAGENTA       	 0xF81F
#define GREEN         	 0x07E0
#define CYAN          	 0x7FFF
#define YELLOW        	 0xFFE0
#define BROWN 			 0XBC40 //棕色
#define BRRED 			 0XFC07 //棕红色
#define GREY  			 0X8430 //灰色
//GUI颜色

#define DARKBLUE      	 0X01CF	//深蓝色
#define LIGHTBLUE      	 0X7D7C	//浅蓝色  
#define GRAYBLUE       	 0X5458 //灰蓝色
//以上三色为PANEL的颜色 

#define LIGHTGREEN     	 0X841F //浅绿色
#define LGRAY 			 0XC618 //浅灰色(PANNEL),窗体背景色

#define LGRAYBLUE        0XA651 //浅灰蓝色(中间层颜色)
#define LBBLUE           0X2B12 //浅棕蓝色(选择条目的反色)

void LCD_Init();
void point_color(u16 color);
void back_color(u16 color);
void LCD_DrawPoint(u16 x, u16 y);
void LCD_point_u16(u16 x, u16 y, u16 color565);
void LCD_DrawPoint_big(u16 x, u16 y);
void LCD_DrawLine(u16 x1, u16 y1, u16 x2, u16 y2);
void Draw_Circle(u16 x0, u16 y0, u8 r);
void LCD_DrawRectangle(u16 x1, u16 y1, u16 x2, u16 y2);
void LCD_Fill(u16 xsta, u16 ysta, u16 xend, u16 yend, u16 color);
void LCD_ShowNum(u16 x, u16 y, u32 num, u8 len);
void LCD_ShowString(u16 x, u16 y, const char *p);
void LCD_Clear(u16 Color);

void LCD_point_u16(u16 x, u16 y, u16 color565);
void LCD_point_u8(u16 x, u16 y, u8 gray);
#endif
