#ifndef __KEY_H
#define __KEY_H	 
//#include "sys.h" 
#include "Puse.h" 
//********************************************************************************	 
//本例程只供学习使用，未经许可，不得用于其它任何用途
//YMKV开发板
//按键 驱动代码	   
//项目：妖米视觉
//创建日期:2019/11/5
//版本：V1.0
//版权所有，盗版必究。
//沈阳妖米科技责任有限公司
//********************************************************************************

#define KEY_up 		  PGin(12)   	
#define KEY_down 		PGin(13)		
#define KEY_left 		PGin(11)		
#define KEY_right 	PGin(14)		


void KEY_Init(void);	//IO初始化
u8 key_scan(u8);  		//按键扫描函数	

#endif
