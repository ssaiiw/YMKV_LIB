#ifndef __ALLOC_H_
#define __ALLOC_H_
#include"malloc.h"

//extern void wjq_free_m( void *ap );
//extern void* wjq_malloc_m( unsigned nbytes );
//extern void *wjq_realloc(void *mem_address, unsigned int newsize);

//#define wjq_free(p) {wjq_free_m(p); p = 0;}

//#define wjq_malloc(suze) wjq_malloc_m(n);

#include"Puse.h"

//extern void mymemcpy(void *des,const void *src,u32 n);
//extern void mymemset(void *s,u8 c,u32 count);
//extern void mymemmove(void *des,const void *src,u32 n);
//extern void *mymemchr(const void *str,int c,size_t n);
//extern int mymemcmp(const void *des,const void *src,size_t n);
//extern void *mymalloc(u8 memx,u32 size);			//内存分配(外部调用)
//extern void myfree(u8 memx,void *ptr);  			//内存释放(外部调用)
//extern void *myrealloc(u8 memx,void *ptr,u32 size);//重新分配内存(外部调用)
//extern void *mycalloc(u8 memx,u32 num,u32 size_t);//内存分配(外部调用)

extern u8 zbar_mem;

//#define zbar_memcpy mymemcpy
//#define zbar_memset mymemset
//#define zbar_memmove mymemmove
//#define zbar_memchr mymemchr
//#define zbar_memcmp mymemcmp

#define zbar_memcpy memcpy
#define zbar_memset memset
#define zbar_memmove memmove
#define zbar_memchr memchr
#define zbar_memcmp memcmp
#define zbar_strtol strtol

//extern void *demymalloc(u8 memx,u32 size);			//内存分配(外部调用)
//extern void demyfree(u8 memx,void *ptr);  			//内存释放(外部调用)
//extern void *demyrealloc(u8 memx,void *ptr,u32 size);//重新分配内存(外部调用)
//extern void *demycalloc(u8 memx,u32 num,u32 size_t);//内存分配(外部调用)

#define YMZB_Malloc(size)     malloc(size)
#define YMZB_free(ptr)        free(ptr)
#define YMZB_Calloc(num,size) calloc(num,size)
#define YMZB_realloc(ptr,size) realloc(ptr,size)


#endif

