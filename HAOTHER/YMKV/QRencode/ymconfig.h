#ifndef YM_CONFIG
#define YM_CONFIG

#include"Puse.h"
//用户调用函数
extern void myfree(u8 memx,void *ptr);  			//内存释放(外部调用)
extern void *mymalloc(u8 memx,u32 size);			//内存分配(外部调用)
extern void *myrealloc(u8 memx,void *ptr,u32 size);//重新分配内存(外部调用)
extern void *mycalloc(u8 memx,u32 num,u32 size_t);//内存分配(外部调用)

extern u8 qrencode_mem;

#define YM_STATIC static
//#define YM_Malloc(size)     mymalloc(qrencode_mem,size)
//#define YM_free(ptr)        myfree(qrencode_mem,ptr) 
//#define YM_Calloc(num,size) mycalloc(qrencode_mem,num,size);
#define YM_Malloc(size)     malloc(size)
#define YM_free(ptr)        free(ptr) 
#define YM_Calloc(num,size) calloc(num,size);
#endif

