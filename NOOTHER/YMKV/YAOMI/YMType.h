#ifndef _YM_TYPE_H
#define _YM_TYPE_H
#include "Puse.h"
#include "YMDef.h"
	
typedef struct featuretype_
{
	u8 Threshold;
	u8 type;
	void (*free)(void *pthis);
	void (*init_to)(void *pthis,u8 what_kernel,...);
}featuretype;
typedef featuretype* FeatureType ;

typedef struct heapdata_
{
    struct heapdata_* last;
    struct heapdata_* next;
    u16 w;
    u16 h;
    u16 value;
}heapdata;
typedef heapdata* HeapDataType;

typedef struct myheaptype_
{
    HeapDataType Head;
    u16 num;
	void (*free)(void *pthis);
	void (*init)(u16 sizenum,...);
}myheaptype;
typedef myheaptype* HeapType;

typedef struct mylistype_
{
    struct mylistype_* lastlist;
    u16 wp;
    u16 hp;
	struct mylistype_* (*free)(void *pthis);
	void (*init)(void *pthis,u16 w,u16 h);
}listype;
typedef listype* ListType;

typedef struct mylinestype_
{
    struct mylinestype_* lastline;
    u16 *xy;
	struct mylinestype_* (*free)(void *pthis);
	void (*init)(void *pthis,u16 *xy);
}lintype;
typedef lintype* LinesType;

typedef struct kerneltype_
{
	u8 R_size;
	float sgm;
	float sgmgray;
	float* data;
	float* (*malloc)(void *pthis);
	void (*free)(void *pthis);
	void (*init)(void *pthis,u8 what_kernel,...);
}kerneltype;
typedef kerneltype* KernelType ;

typedef struct edgekerneltype_
{
	u8 type;
	u8 canythremax;
	u8 canythremin;
	void (*free)(void *pthis);
}edgekerneltype;
typedef edgekerneltype* EdgeKernelType ;

typedef struct fftfiltertype_
{
    u8 type;
	u16 D0;
	u8 n;
	void (*free)(void *pthis);

}fftfiltertype;
typedef fftfiltertype* FFTFilterType ;

typedef struct dwtfiltertype_
{
    u8 type;
	u8 num;
	float threshold;
	void (*free)(void *pthis);

}dwtfiltertype;
typedef dwtfiltertype* DWTFilterType ;

typedef struct dvectortype_
{
    int len_size;
	float* data;
	float* (*malloc)(void *pthis);
	void (*free)(void *pthis);
}dvectortype;
typedef dvectortype* FVectordType ;

typedef struct vectortype_S
{
    int len_size;
	short* data;
	short* (*malloc)(void *pthis);
	void (*free)(void *pthis);
}svectortype;
typedef svectortype* SVectordType ;

typedef struct featurestype_
{
    u8 xycnum;
	short* xyc;
	u8 featdiscrynum;
	short* featdis;
	struct featurestype_*next;
	void (*init)(void *pthis,u8 xyc_num,u8 featdis_num);
	void (*free)(void *pthis);
}featurestype;
typedef featurestype* Features;

typedef struct featurelistype_
{
    struct featurelistype_* lastlist;
    Features thisfeature;
	void (*featureinit)(void *pthis,u8 xyc_num,u8 featdis_num);
	struct featurelistype_* (*free)(void *pthis);
}featurelistype;
typedef featurelistype* FeatureList;

typedef struct gray2thresholdtype_
{
	short thremin;
	short thremax;
	void (*free)(void *pthis);
}gray2thresholdtype;
typedef gray2thresholdtype* Gray2ThresholdType ;

typedef struct colorthresholdtype_
{
	u8 type;
	Gray2ThresholdType thre1;
	Gray2ThresholdType thre2;
	Gray2ThresholdType thre3;
	void (*free)(void *pthis);
}color2thresholdtype;
typedef color2thresholdtype* Color2ThresholdType ;

typedef struct imgtype_
{
	u16 width;
	u16 height;
	u8 pmlc; //图像完成标识
	size_t sizet;
	u8* data;
	u8* (*malloc)(void *pthis);
	void (*free)(void *pthis);
}imgtype;
typedef imgtype* ImgType ;

typedef struct myblobype_
{
    struct myblobype_* nextlist;
    struct myblobype_* lastlist;
    u16 wp;
    u16 hp;
    u16 width;
    u16 height;
    u8 value;
	  u16 startwp;
    u16 starthp;
    unsigned int area;
	struct myblobype_* (*free)(void *pthis);
	void (*init)(void *pthis,u16 pw,u16 ph,u16 width,u16 height,unsigned int area);
}blobype;
typedef blobype* BlobType;

typedef struct myqrlistype_
{
    struct myqrlistype_* lastlist;
    u16 wp;
    u16 hp;
	  u16 width;
	  u16 height;
	  const char* data;
	  const char* type;
	struct myqrlistype_* (*free)(void *pthis);
	void (*init)(void *pthis,u16 w,u16 h);
}qrlistype;
typedef qrlistype* QRListType;

typedef struct showtype_
{
	void (*gray)(u16 x,u16 y,ImgType Gray);
	void (*rgb565)(u16 x,u16 y,ImgType Rgb565);
  void (*IMG)(u16 x,u16 y,ImgType IMG);
	void (*pixq)(int startx,int starty ,int size);
	void (*Hist)(ImgType gray,int startx,int starty ,int width);
	void (*qrlist)(u16 x,u16 y,QRListType pthis);
}showtype;

typedef struct crack_feature_
{
    u16 crack_area;//面积
    u16 crack_len;//长度
    void (*init_len)(void *pthis,ImgType binary,BlobType thisblob);
    u16 crack_width;//宽度
    void (*init_width)(void *pthis,ImgType binary,BlobType thisblob);
    u16 starti,startj;
    u16 i,j,width,height;
    u16 bone_num;//骨架
    u16* bonei;
    u16* bonej;
    void (*init_bone)(void *pthis,ImgType binary,BlobType thisblob);
    u16 edge_num;//边界
    u16* edgei;
    u16* edgej;
    void (*init_edge)(void *pthis,ImgType binary,BlobType thisblob);
	void* (*malloc)(size_t num);
	void (*free)(void *pthis);
}crack_feature;
typedef crack_feature* Crack_Type;

typedef struct KVtype_
{
		void (*rgb2gray)(ImgType Rgb565,ImgType Gray);
		void (*rgbexpose)(ImgType Rgbin,ImgType Rgbout);
		void (*colour)(ImgType Grayin,ImgType RGBout,u8 rR,u8 rG,u8 rB);
    void (*binary)(ImgType Grayin,ImgType Grayout,u8 threshold);
    void (*stretch)(ImgType imgin,ImgType imgout);
    void (*stretch_linera)(ImgType imgin,ImgType imgout);
    void (*rotate)(ImgType imgin,ImgType imgout,float angle);
    void (*bright)(ImgType imgin,float bright);
    void (*invers)(ImgType imgin,float bright);
    void (*contract)(ImgType imgin,short value);
    void (*Enhance_Liner)(ImgType imgin,u8 a,u8  b,u8  c);
    void (*Enhance_Log)(ImgType imgin,float a,float b,float c);
    void (*Enhance_Exp)(ImgType imgin,float a,float b,float c);
    void (*Enhance_Gama)(ImgType imgin,float c,float r);
    void (*Enhance_Scurve)(ImgType imgin,float m,float E);
    void (*His_eq)(ImgType imgin);
    void (*His_get)(ImgType imgin,int myHist[256]);
    void (*His_to)(ImgType imgin,int statis[256]);
    ImgType (*conv)(KernelType myker,ImgType myimg);
    ImgType (*filter)(u8 Method,KernelType mykernel,ImgType myimg);
    void (*fft)(float preal[],float pimag[],int n,float freal[],float fimag[]);
    void (*ifft)(float preal[],float pimag[],int n,float freal[],float fimag[]);
    void (*fft2)(ImgType myimg,FVectordType imgr,FVectordType imgi);
    void (*ifft2)(ImgType myimg,FVectordType imgr,FVectordType imgi);
    void (*fftshift2)(ImgType myimg,FVectordType imgr,FVectordType imgi);
    void (*fftfilter)(ImgType myimg,FVectordType imgr,FVectordType imgi,FFTFilterType myfilter);
    void (*dwt)(float *pdata,int num);
    void (*idwt)(float *pdata,int num);
    void (*dwt2)(ImgType myimg,FVectordType dwtout,DWTFilterType myfilter);
    void (*idwt2)(ImgType myimg,FVectordType dwtout,DWTFilterType myfilter);
    void (*dwtfilter)(ImgType myimg,FVectordType dwtout,DWTFilterType myfilter);
    void (*threshold2g)(ImgType myimg,Gray2ThresholdType mythre);
    void (*threshold2c)(ImgType myimg,ImgType binaryimg,Color2ThresholdType mythre);
    void (*edge)(ImgType myimg,EdgeKernelType edgeker);
    SVectordType (*houghline)(ImgType myimg,u8 numthreshold,u8 Linenum);
    void (*region)(ImgType myimg,u16 x,u16 y,u8 detathreshold);
    ImgType (*morp)(ImgType myimg,u8 r,u8 morptype);
    void (*dis_tranfrom)(ImgType myimg);
    BlobType (*blobs_find)(ImgType myimg,int minarea);
    BlobType (*temp_find)(ImgType myimg,ImgType temp,u8 error_max);
    void (*thin)(ImgType myimg);
    FeatureList (*feature_point)(ImgType myimg,FeatureType point_type);
    ImgType (*feature_img)(ImgType myimg,FeatureType point_type);
		QRListType (*find_code)(ImgType myimg);
    BlobType (*colorblobs_find)(ImgType myimg,u8 thremaxmin[],u8 type,u8 num,int minarea);
		ImgType (*blobimg)(ImgType myimg,BlobType pthis);
    void (*bone_cut)(ImgType myimg,u16 threshold);
    ImgType (*edge_trace)(ImgType myimg,BlobType pthis);
}KVtype;

typedef struct creattype_
{
    ImgType (*img)(u16 width,u16 height,ImgUn Imgform);
    KernelType (*kernel)(u8 R_size);
    FVectordType (*fVetcor)(int sizenum);
    SVectordType (*sVetcor)(int sizenum);
    FFTFilterType (*fftfilter)(u8 mytype,u16 myD0, u8 gsn);
    DWTFilterType (*dwtfilter)(u8 mytype, u8 grade_num,float minthre);
    Gray2ThresholdType (*gray2thre)(short thre_max,short thre_min);
    Color2ThresholdType (*color2thre)(short threshold[6],u8 type);
    ImgType (*ImgCopy)(ImgType myimg,u16 startw,u16 starth,u16 width,u16 height);
	  ImgType (*ImgBackup)(ImgType myimg);
    ImgType (*data2img)(u8* data,u16 width,u16 height,ImgUn Imgform);
    EdgeKernelType (*edgeker)(u8 type,...);
    ListType (*alist)(ListType last);
    ListType* (*alist_s)(int num);
	  LinesType (*aline)(LinesType last);
	  void (*free_lines)(LinesType lines);
    void (*free_alist_s)(ListType* pthis,int num);
    void (*free_blobs)(BlobType Tail);
    BlobType (*blob)(BlobType last);
    FeatureType (*feature)(u8 feature_point_type,...);
    FeatureList (*features_Lis)(FeatureList last);
	  void (*free_feat_Lis)(FeatureList myfeature);
	  ImgType (*QR_encode)(const char* str,u8 size);
		QRListType (*qrlist)(QRListType last);
		void (*free_qrlist)(QRListType pthis);
}Creattype;

typedef struct YMKVtype_
{
	u8 imgok;
	showtype* show;
	KVtype *KV;
	void (*doing)();

}YMKVtype;




#endif

