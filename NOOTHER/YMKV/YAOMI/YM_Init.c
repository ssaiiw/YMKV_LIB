#include "YMKV.h"
#include "YMCreat.h"
#include "other.h"
#include "YMKV_other.h"
//#include "YMCreatadd.h"
/***************************************

功能：    挂载初始化

****************************************/

void YMKV_Init()
{
	// 显示  行为 初始化
	myshow.gray = YMKV_show_gray2;
	myshow.rgb565 = YMKV_show_pic2;
	myshow.IMG = YMKV_show;
	myshow.pixq = qshow;
	myshow.Hist = Histshow;
	myshow.qrlist = qrlist_show;

	//算法    初始化
	myKV.rgb2gray = YMKV_pic_rgb565togray2;
	myKV.rgbexpose = YMKV_pic_rgb565exposure2;
	myKV.colour = YMKV_pic_color_to2;
	myKV.binary = YMKV_pic_graytobinary2;
	myKV.stretch = YMKV_pic_stretch;
	myKV.stretch_linera = YMKV_pic_stretch_linera;
	myKV.rotate = YMKV_pic_rotate;
	myKV.bright = YMKV_pic_Bright;
	myKV.invers = YMKV_pic_Invers;
	myKV.contract = YMKV_pic_Contract;
	myKV.Enhance_Liner = YMKV_pic_Liner_Enhance;
	myKV.Enhance_Log = YMKV_pic_grayLog_Enhance;
	myKV.Enhance_Exp = YMKV_pic_rgb565Exp_Enhance;
	myKV.Enhance_Gama = YMKV_pic_Gama_Enhance;
	myKV.Enhance_Scurve = YMKV_pic_Scurve_Enhance;
	myKV.His_get = YMKV_gray_His_get;
	myKV.His_eq = YMKV_gray_His_eq;
	myKV.His_to = YMKV_gray_His_to;
	myKV.conv = YMKV_pic_grayConvolution;
	myKV.filter = YMKV_pic_grayFilter;
	myKV.fft = YMKV_Ymfft;
	myKV.ifft = YMKV_Ymifft;
	myKV.fft2 = YMKV_gray_FFT;
	myKV.ifft2 = YMKV_gray_IFFT;
	myKV.fftshift2 = YMKV_gray_FFTshift;
	myKV.fftfilter = YMKV_YMFFTfilter;
	myKV.dwt = YMKV_YmDwT;
	myKV.idwt = YMKV_YmIDwT;
	myKV.dwt2 = YMKV_gray_DWT;
	myKV.idwt2 = YMKV_gray_IDWT;
	myKV.dwtfilter = YMKV_gray_DWT_filter;
	myKV.threshold2g = YMKV_gray_2threshold;
	myKV.threshold2c = YMKV_color_2threshold;
	myKV.edge = YMKV_gray_edge;
	myKV.houghline = YMKV_binary_hough_line;
	myKV.region = YMKV_gray_region_growing;
	myKV.morp = YMKV_gray_morphology;
	myKV.dis_tranfrom = YMKV_binary_dis_transform;
	myKV.blobs_find = YMKV_findblob;
	myKV.temp_find = YMKV_match_template;
	myKV.thin = YMKV_binary_thin;
	myKV.feature_point = YMKV_find_feature_point;
	myKV.feature_img = YMKV_img_features;
	myKV.find_code = YMKV_Zbar_find;
	myKV.colorblobs_find = YMKV_findcolorblobs;
	myKV.blobimg = YMKV_blobimg;
	myKV.bone_cut = YMKV_cut_bone;
	myKV.edge_trace = YMKV_trace_edge;

	// creat 初始化
	Creat.img = YMKV_Creat_img_creat;
	Creat.fVetcor = YMKV_Creat_fVetcor_creat;
	Creat.kernel = YMKV_Creat_kernel_creat;
	Creat.dwtfilter = YMKV_Creat_dwtfilter_creat;
	Creat.fftfilter = YMKV_Creat_fftfilter_creat;
	Creat.gray2thre = YMKV_Creat_gray2threshold_creat;
	Creat.color2thre = YMKV_Creat_color2threshold_creat;
	Creat.data2img = YMKV_Creat_DataToImg;
	Creat.ImgCopy = YMKV_Creat_ImgCopyto;
	Creat.ImgBackup = YMKV_Creat_Img_Backup;
	Creat.sVetcor = YMKV_Creat_SVetcor_creat;
	Creat.edgeker = YMKV_Creat_edgeker_creat;
	Creat.alist = YMKV_Creat_alist_creat;
	Creat.alist_s = YMKV_Creat_liststrmalloc;
	Creat.free_alist_s = YMKV_Creat_liststrfree;
	Creat.aline = YMKV_Creat_aline_creat;
	Creat.free_lines = YMKV_Creat_linestrfree;
	Creat.blob = YMKV_Creat_blob_creat;
	Creat.free_blobs = YMKV_Creat_blobsfree;
	Creat.feature = YMKV_Creat_feature_creat;
	Creat.features_Lis = YMKV_Creat_Feature_list_creat;
	Creat.free_feat_Lis = YMKV_Creat_featlists_free;
	Creat.QR_encode = YMKV_QR_encode;
	Creat.qrlist = YMKV_Creat_qrlist_creat;
	Creat.free_qrlist = YMKV_Creat_qrlist_free;

	// creatadd 初始化
	//Creatadd.crack_feature=YMKV_crack_feature_creat;
	//Creatadd.free_crack=YMKV_crack_destory_creat;

// YMKV 初始化
	YMKV.show = &myshow;
	YMKV.KV = &myKV;
	YMKV.doing = YMKVdo;
}


