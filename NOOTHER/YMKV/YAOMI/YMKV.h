#ifndef ymkv_h
#define ymkv_h

//********************************************************************************	 
//本例程只供学习使用，未经许可，不得用于其它任何用途
//YMKV开发板
//图像处理算法代码	   
//项目：妖米视觉
//创建日期:2019/11/5
//版本：V1.0
//版权所有，盗版必究。
//沈阳妖米科技责任有限公司
//********************************************************************************

#include "graph.h"
#include "stdlib.h"
#include "math.h"
#include "YMType.h"
#include "YMDef.h"

extern u16 tem_deta;
extern u8 zbar_mem;
extern u8 qrencode_mem;
extern showtype myshow;
extern KVtype myKV;
extern YMKVtype YMKV;
extern Creattype Creat;
extern u8 pix2pix[256];
extern u8 Liner_Enhance_a;
extern u8 Liner_Enhance_b;
extern u8 Liner_Enhance_c;
extern int statistic[256];//直方图

void YMKV_Init(void);
void YMKVdo(void);
extern void YMKV_show_pic(u16 x,u16 y,u16 Width,u16 Height,u16 *pic);//RGB565格式图像在LCD上显示
extern void YMKV_show_gray(u16 x,u16 y,u16 Width,u16 Height,u8 *pic);//灰度图像在LCD上显示
void YMKV_show_gray2(u16 x,u16 y,ImgType showImg);
void YMKV_show_pic2(u16 x,u16 y,ImgType showImg);
void YMKV_show(u16 x,u16 y,ImgType showImg);

//　　　　　　　　　函数原型
void YMKV_rgb565toRGB3(u16 rgb565,u8 *R,u8 *G,u8 *B);//   将RGB565格式的像素点提取出三通道RGB
void YMKV_rgb565toRGB_3(u16 rgb565,u8 *RGB);//   将RGB565格式的像素点提取出三通道RGB
u16  YMKV_RGB3torgb565(u8 R,u8 G,u8 B);//   将三通道RGB图的像素点转化为RGB565格式输出
u16  YMKV_graytorgb565(u8 gray);//   灰度像素转彩色像素
u8 YMKV_rgb565togray(u16 color565);//彩色像素转灰度像素
void YMKV_rgb565toLABYMKV_Lab2RGB(u16 rgb565,short *L,short *A,short *B);//   rgb转lab
void YMKV_Lab2RGB(u16* rgb565, u8 L,u8 A,u8 B);//LAB转RGB
void YMKV_rgb565toHSV(u16 rgb565,short *H,short *S,short *V);//RGB转HSV
u8   YMKV_graytobinary(u8 gray,u8 threshold);//将灰度格式的像素点转化为二值图输出
u8   YMKV_graytobinary_2threshold(short gray,short thresholdmin,short thresholdmax);//将灰度像素点双阈值分割为二值
void YMKV_pic_rgb565togray(u16* scr,u8* output,u16 Width,u16 Height);//RGB565图片转灰度图
void YMKV_pic_rgb565exposure(u16* scr,u16* output,u16 Width,u16 Height);//图片曝光处理
void YMKV_pic_color_to(u8* gray_pic,u16* rgb,u16 Width,u16 Height,u8 rR,u8 rG,u8 rB);//          图片染色
void YMKV_pic_graytobinary(u8* scr,u8* output,u8 threshold,u16 Width,u16 Height);//转二值图

void YMKV_pic_rgb565togray2(ImgType Rgb565,ImgType Gray);//将RGB565格式的图片转化为灰度图
void YMKV_pic_rgb565exposure2(ImgType Rgbin,ImgType Rgbout);//将RGB565格式的图片曝光处理
void YMKV_pic_color_to2(ImgType Grayin,ImgType RGBout,u8 rR,u8 rG,u8 rB);//将灰度图片染为彩色
void YMKV_pic_graytobinary2(ImgType Grayin,ImgType Grayout,u8 threshold);//将灰度的图片转化为二值图
void YMKV_pic_stretch(ImgType imgin,ImgType imgout);//图像邻近内插拉伸与压缩
void YMKV_pic_stretch_linera(ImgType imgin,ImgType imgout);//图像双线性内插拉伸与压缩
void YMKV_pic_rotate(ImgType imgin,ImgType imgout,float angle);//图像旋转变换 angle:顺时针旋转角度
void YMKV_pic_Bright(ImgType imgin,float bright);//亮度增强 bright:亮度增益系数  范围:0~5
void YMKV_pic_Invers(ImgType imgin,float bright);//图像反色变换 bright:灰度反色增益系数  范围:0~5
void YMKV_pic_Contract(ImgType imgin,short value);//图像对比度增强  value:对比度增强系数 范围:-50~50
void YMKV_pic_Liner_Enhance(ImgType imgin,u8 a,u8  b,u8  c);//图像分段线性增强 [0,a] [a,b] [b,c] [c,255]
void YMKV_pic_grayLog_Enhance(ImgType imgin,float a,float b,float c);//图像log变换非线性增强
void YMKV_pic_rgb565Exp_Enhance(ImgType imgin,float a,float b,float c);//图像exp指数变换非线性增强
void YMKV_pic_Gama_Enhance(ImgType imgin,float c,float r);//图像增强Gama非线性变换
void YMKV_pic_Scurve_Enhance(ImgType imgin,float m,float E);//图像对比度拉伸   S型曲线增强
void YMKV_gray_His_get(ImgType imgin,int myHist[256]);//图像灰度直方图获取
void YMKV_gray_His_eq(ImgType imgin);//图像灰度直方图均衡化
void YMKV_gray_His_to(ImgType imgin,int statis[256]);//图像灰度直方图规定化
float YMKV_Averkernel(int x,int y);//均值核输出值
float YMKV_Gausskernel(float Gausssgm,int x,int y);//高斯核输出值
float YMKV_LoGkernel(float Gausssgm,int x,int y);//LOG滤波核

void Kernel_init(void *pthis,u8 what_kernel,...);//产生半径为r的滤波核 默认为8邻域
ImgType YMKV_pic_grayConvolution(KernelType myker,ImgType myimg);// 利用半径为r的核对图像进行卷积操作
ImgType YMKV_pic_grayFilter(u8 Method,KernelType myker,ImgType myimg);//图像滤波
void YMKV_Ymfft(float preal[],float pimag[],int n,float freal[],float fimag[]);//一维傅里叶变换
void YMKV_Ymifft(float preal[],float pimag[],int n,float freal[],float fimag[]);//一维傅里叶逆变换
void YMKV_gray_FFT(ImgType myimg,FVectordType imgr,FVectordType imgi);//二维图像FFT
void YMKV_gray_IFFT(ImgType myimg,FVectordType imgr,FVectordType imgi);//二维图像FFT
void YMKV_gray_FFTshift(ImgType myimg,FVectordType imgr,FVectordType imgi);//二维图像FFT零频点移动到中心
void YMKV_YMFFTfilter(ImgType myimg,FVectordType imgr,FVectordType imgi,FFTFilterType myfilter);//图像频域滤波
void YMKV_YmDwT(float dwtout[],int n);// 一维维离散小波变换
void YMKV_YmIDwT(float dwtout[],int n);// 一维维离散小波逆变换
void YMKV_gray_DWT(ImgType myimg,FVectordType dwtout,DWTFilterType myfilter);//二维图像小波变换
void YMKV_gray_IDWT(ImgType myimg,FVectordType dwtout,DWTFilterType myfilter);//二维离散小波恢复（单通道浮点图像）
void YMKV_gray_DWT_filter(ImgType myimg,FVectordType dwtout,DWTFilterType myfilter);//harr小波硬阈值去噪
void YMKV_gray_2threshold(ImgType myimg,Gray2ThresholdType mythre);//图像灰度双阈值分割
void YMKV_color_2threshold(ImgType myimg,ImgType binaryimg,Color2ThresholdType mythre);//图像颜色空间双阈值分割
void YMKV_pic_grayConvolution_S(KernelType myker,ImgType myimg,short *out);//图像卷积
void YMKV_gray_edge(ImgType myimg,EdgeKernelType edgekertype);//图像边缘提取
SVectordType YMKV_binary_hough_line(ImgType myimg,u8 numthreshold,u8 Linenum);//图像霍夫变换 输出检测到的直线rou-theta参数
void YMKV_gray_region_growing(ImgType myimg,u16 w,u16 h, u8 Detathreshold);//图像区域生长
u8 YMKV_Hmin(short *Hist);//直方图求最小值
u8 YMKV_Hmedian(short *Hist,int med_num);//       直方图求中值
u8 YMKV_Hmax(short *Hist);//       直方图求最大值
ImgType YMKV_gray_morphology(ImgType myimg,u8 r,u8 morptype);//形态学运算
void YMKV_binary_dis_transform(ImgType myimg);//二值图距离变换
BlobType YMKV_findblob(ImgType myimg,int minarea);//二值图色块查找
BlobType YMKV_match_template(ImgType myimg,ImgType temp,u8 error_min);//模板匹配
void YMKV_binary_thin(ImgType myimg);//骨架细化
BlobType YMKV_findcolorblobs(ImgType myimg,u8 thremaxmin[],u8 type,u8 num,int minarea);//多色块查找
ImgType YMKV_GSConvolutionF(KernelType myker,ImgType myimg);//高斯卷积优化
void YMKV_imgf2g(ImgType imgf,ImgType imgg);//浮点转整形
void YMKV_imgdog(ImgType out,ImgType img);//DOG运算
FeatureList YMKV_find_feature_point(ImgType myimg,FeatureType point_type);//找特征点
ImgType YMKV_img_features(ImgType myimg,FeatureType point_type);//特征图

ImgType YMKV_blobimg(ImgType myimg,BlobType pthis);//色块面积过滤图
void YMKV_cut_bone(ImgType myimg,u16 threshold);//骨架剪枝
ImgType YMKV_trace_edge(ImgType myimg,BlobType pthis);//边界跟踪


#endif
