#ifndef _YM_EDF_H
#define _YM_EDF_H
#include "Puse.h"
#define OK 1 


#define pix2use 
#define Methuse //�㷨��
#define KVdouse 

#define pai 3.1415926f
#define deg2rad (pai/180)
#define rad2deg (180/pai) 
#define Null 0
#define Aver 1
#define Gauss 2
#define LOG 3
#define MedFilter 4
#define BilFilter 5
#define OtherFilter 2
#define GaussD 2
#define GaussG 6
#define ButterworthD 7
#define ButterworthG 8
#define Harr 9
#define GRAY 9
#define RGB3 10
#define LAB 11
#define Sobel 12
#define Canny 13
#define Laplace 14
#define Morp_dilate 15
#define Morp_erode 16
#define SUSAN 17
#define Harris 18
#define LBP 19
#define MBLBP 20

#define HfLT 180

#define pixbad 1//��������
//typedef u32     size_t;
#define ImgUn size_t
#define ImgEdge u8
#define imgGray sizeof(u8)
#define imgRGB sizeof(u16)
#define imgFloat sizeof(float)
	

#endif
