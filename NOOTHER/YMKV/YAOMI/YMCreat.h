#ifndef _YM_CREAT_H
#define _YM_CREAT_H
#include "Puse.h"
#include "YMType.h"

void YMKV_Creat_mylist_init(void *pthis,u16 w,u16 h);
u8* YMKV_Creat_imgmalloc(void *pthis);
void YMKV_Creat_imgfree(void *pthis);
float* YMKV_Creat_kernelmalloc(void *pthis);
void YMKV_Creat_kernelfree(void *pthis);
void YMKV_Creat_edgekernelfree(void *pthis);
float* YMKV_Creat_fvectormalloc(void *pthis);
void YMKV_Creat_fvectorfree(void *pthis);
short* YMKV_Creat_svectormalloc(void *pthis);
BlobType YMKV_Creat_blobfree(void *pthis);
void YMKV_Creat_blobInit(void *pthis,u16 pw,u16 ph,u16 width,u16 height,unsigned int area);
void YMKV_Creat_svectorfree(void *pthis);
void YMKV_Creat_fftfilterfree(void *pthis);
void YMKV_Creat_dwtfilterfree(void *pthis);
void YMKV_Creat_Gray2Threfree(void *pthis);
void YMKV_Creat_Color2Threfree(void *pthis);
void YMKV_Creat_Kernel_init(void *pthis,u8 what_kernel,...);

ListType YMKV_Creat_mylist_free(void *pthis);
ListType* YMKV_Creat_liststrmalloc(int num);
QRListType YMKV_Creat_qrlist_creat(QRListType last);
void YMKV_Creat_qrlist_free(QRListType pthis);
void YMKV_Creat_liststrfree(ListType* pthis,int num);
void YMKV_Creat_feature_free(void* pthis);
void YMKV_Creat_feature_init(FeatureType pthis,...);
void YMKV_Creat_feats_free(void *pthis);
void YMKV_Creat_features_creat(void*pthis, u8 xyc_num,u8 featdis_num);
FeatureList YMKV_Creat_featlis_free(void *pthis);
FeatureList YMKV_Creat_Feature_list_creat(FeatureList last);
ListType YMKV_Creat_alist_creat(ListType last);
LinesType YMKV_Creat_aline_creat(LinesType last);
void YMKV_Creat_linestrfree(LinesType pthis);
FeatureType YMKV_Creat_feature_creat(u8 feature_point_type,...);
void YMKV_Creat_featlists_free(FeatureList myfeature);
BlobType YMKV_Creat_blob_creat( BlobType lastblob);
void YMKV_Creat_blobsfree(BlobType pthis);
ImgType YMKV_Creat_img_creat(u16 width,u16 height,ImgUn Imgform);
KernelType YMKV_Creat_kernel_creat(u8 R_size);
EdgeKernelType YMKV_Creat_edgeker_creat(u8 type,...);
FVectordType YMKV_Creat_fVetcor_creat(int sizenum);
SVectordType YMKV_Creat_SVetcor_creat(int sizenum);
FFTFilterType YMKV_Creat_fftfilter_creat(u8 mytype,u16 myD0, u8 gsn);
DWTFilterType YMKV_Creat_dwtfilter_creat(u8 mytype, u8 grade_num,float minxs);
Gray2ThresholdType YMKV_Creat_gray2threshold_creat(short thre_max,short thre_min);
Color2ThresholdType YMKV_Creat_color2threshold_creat(short threshold[6],u8 type);
ImgType YMKV_Creat_ImgCopyto(ImgType myimg,u16 startw,u16 starth,u16 width,u16 height);
ImgType YMKV_Creat_DataToImg(u8* data,u16 width,u16 height,ImgUn Imgform);
ImgType YMKV_Creat_Img_Backup(ImgType myimg);



#endif

