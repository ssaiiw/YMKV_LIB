#include "YMKV.h"
#include "camera.h"
#include "other.h"
#include "stdio.h"

char show[20];

void YMKVdo()
{
	ImgType grayd;
	u8 keyvalue;
	int i;
	static u8 k = 0;
	static short r[2] = { 1,1 };
	//100,39,219,40,223,54
	u8 thremaxmin[] = { 100,39,115,65,170,136,  100,39,219,148,223,136,
									 100,39,219,40,114,108,	 100,39,219,40,111,91,
									 100,39,219,40,91,54,	   100,39,115,65,227,176 };
	BlobType myblob = Null;

	if (YMKV.imgok == OK)
	{
		grayd = Creat.img(CAM->width, CAM->height, imgGray);//灰度图申请

		YMKV.KV->rgb2gray(CAM, grayd);//转灰度图
		graph.eye->go();//开始获取下一帧
		YMKV.show->IMG(40, 20, CAM);//显示
		myblob = YMKV.KV->colorblobs_find(CAM, thremaxmin, LAB, 6, 300);//色块查找(ImgType myimg,u8 thremaxmin[],u8 type,u8 num,int minarea)
		draw_blobs(40, 20, myblob);//画出色块


		WINDOW->update(WINDOW);//更新显示窗口内容
		keyvalue = graph.key->scan(1);
		system("cls");//清空控制台显示
		//graph.lcd->clear(RED);
		//WINDOW->setImg(WINDOW, CAM);

		switch (keyvalue)
		{
		case Upm: r[k] += 1; break;
		case Downm: r[k] -= r[k] > 0 ? 1 : 0; break;
		case Leftm:if (k <= 0)k = 0; else k--; break;
		case Rightm:if (k >= 1)k = 0; else k++; break;
		}
		printf("\n k=%d", k);
		grayd->free(grayd);//图片内存释放
		Creat.free_blobs(myblob);
	}
}



