#ifndef YMKV_OTHER_H
#define YMKV_OTHER_H

#include "YMType.h"


ImgType YMKV_QR_encode(const char* str,u8 size);
QRListType YMKV_Zbar_find(ImgType myimg);

#endif
