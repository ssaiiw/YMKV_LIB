#pragma once

#include<windows.h>
#include "Puse.h"
#include "YMType.h"
//#define ImgType void* 


extern HWND hWnd;
extern HDC   hDC;


typedef struct mywindow {
	HWND hWnd;
	HDC hDc;

	HBITMAP hBmp;
	int x, y, width, height;
	unsigned char* imdata;
	void(*init_img)();
	void(*setXY)(void* pthiswin, int x, int y);
	void(*setImg)(void* pthiswin, ImgType myimg);
	void(*free)(void* pthiswindow);
	void(*update)(void* pthiswindow);
} mywindow;
typedef struct mywindow* MyWindow;

extern MyWindow WINDOW;

typedef struct wincreat {
	MyWindow(*NewWin)(int width, int height);
};

extern struct wincreat WinCreat;

