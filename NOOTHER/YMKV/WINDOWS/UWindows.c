
#include "UWindows.h"
#include "other.h"
#include "lcd.h"

HWND hWnd;
HDC   hDC;
MyWindow WINDOW;

MyWindow create_my_window(int width, int height);
void show_my_window(void* pthiswin);
void destroy_my_window(void* pthiswin);
void set_my_window(void* pthiswin, int x, int y);
void set_my_Img(void* pthiswin, ImgType myimg);

struct wincreat WinCreat = {
	create_my_window, //创建窗口函数



};
void destroy_my_window(void* pthiswin) {
	MyWindow my_window = pthiswin;
	if (my_window != Null) {
		if (my_window->imdata != Null)
			free(my_window->imdata);
		free(my_window);

		DeleteObject(my_window->hBmp);//释放资源

	}
	my_window = Null;
}
MyWindow create_my_window(int width, int height) {
	MyWindow my_window = (MyWindow)malloc(sizeof(mywindow));
	my_window->x = 0;
	my_window->y = 0;
	my_window->width = width;
	my_window->height = height;
	my_window->hDc = hDC;
	my_window->hWnd = hWnd;
	my_window->imdata = (unsigned char *)malloc(width*height * 4);
	my_window->free = destroy_my_window;
	my_window->update = show_my_window;
	my_window->setImg = set_my_Img;
	my_window->setXY = set_my_window;

	int pos = 0;
	for (int x = 0; x < height; x++)
	{
		for (int y = 0; y < width; y++)
		{
			my_window->imdata[pos++] = 255;//B
			my_window->imdata[pos++] = 255;//G
			my_window->imdata[pos++] = 255;//R
			my_window->imdata[pos++] = 0;//alpha  
		}
	}
	//创建一个位图
	my_window->hBmp = CreateBitmap(width, height, 32, 1, my_window->imdata);
	//my_window->hBmp = NULL;
	return my_window;
}
//显示该窗口
void show_my_window(void* pthiswin) {
	MyWindow my_window = pthiswin;
	HDC hdc = my_window->hDc;
	/*system("cls");*/
	//LockWindowUpdate(NULL);//解锁窗口
	HDC hdc1 = CreateCompatibleDC(hdc); //创建画布hdc1

	//更新BMP
	DeleteObject(my_window->hBmp);//释放资源
	my_window->hBmp = CreateBitmap(my_window->width, my_window->height, 32, 1, my_window->imdata);  //将缓冲数据创建为一个位图

	BITMAP bm;
	GetObject(my_window->hBmp, sizeof(bm), &bm);//得到一个位图对象 bm
	SelectObject(hdc1, my_window->hBmp);//在hdc1上绘图
	BitBlt(hdc, my_window->x, my_window->y, my_window->width, my_window->height, hdc1, 0, 0, SRCCOPY);//显示位图 将hdc1的图像显示出来

	DeleteDC(hdc1);//删除画布
	//LockWindowUpdate(my_window->hWnd);//锁定窗口
}
//设置窗口坐标
void set_my_window(void* pthiswin, int x, int y)
{
	MyWindow my_window = pthiswin;
	my_window->x = x;
	my_window->y = y;
}
//设置窗口尺寸
void set_my_window_size(void* pthiswin, int width, int height)
{
	MyWindow my_window = pthiswin;
	my_window->height = height;//更新窗口宽高
	my_window->width = width;
	free(my_window->imdata);
	my_window->imdata = (unsigned char *)malloc(width*height * 4);
}

//设置窗口图片
void set_my_Img(void* pthiswin, ImgType myimg)
{
	MyWindow my_window = pthiswin;
	int height = myimg->height;
	int width = myimg->width;


	if ((my_window->height != height) || (my_window->width != width))
	{
		set_my_window_size(my_window, width, height);
	}
	int pos = 0;
	int pp = 0;
	u16* data = (u16*)myimg->data;
	u8* datag = myimg->data;
	switch (myimg->sizet)
	{
	case imgRGB:
		for (int x = 0; x < height; x++)
		{
			for (int y = 0; y < width; y++)
			{
				pos = 4 * pp;
				u16 rgb565 = data[pp];
				my_window->imdata[pos] = ((rgb565 & 0x001F) << 3);//B
				my_window->imdata[pos + 1] = ((rgb565 & 0x7E0) >> 3);//G
				my_window->imdata[pos + 2] = ((rgb565 & 0xF800) >> 8);//R
				my_window->imdata[pos + 3] = 0;//alpha
				pp++;
			}
		}
		break;
	case imgGray:
		for (int x = 0; x < height; x++)
		{
			for (int y = 0; y < width; y++)
			{
				pos = 4 * pp;
				u8 grayc = datag[pp];
				my_window->imdata[pos] = grayc;//B
				my_window->imdata[pos + 1] = grayc;//G
				my_window->imdata[pos + 2] = grayc;//R
				my_window->imdata[pos + 3] = 0;//alpha
				pp++;
			}
		}
		break;
	default:
		break;
	}

}


void point_color(u16 color)
{
	POINT_COLOR = color;
}
void back_color(u16 color)
{
	BACK_COLOR = color;
}




