#ifndef graph_h
#define graph_h
#include"Puse.h"
#include "UWindows.h"
#include"lcd.h"

//********************************************************************************	 
//本例程只供学习使用，未经许可，不得用于其它任何用途
//YMKV开发板
//底层接口配置代码 
//项目：妖米视觉
//创建日期:2019/11/5
//版本：V1.0
//版权所有，盗版必究。
//沈阳妖米科技责任有限公司
//********************************************************************************


#define Upm     'U'
#define Downm   'D'
#define Leftm	 'L'
#define Rightm  'R'
#define Pr_N 1
#define Pr_1 0

typedef struct lcdtype_
{
	void(*init)();
	void(*point_color)(u16 color);
	void(*back_color)(u16 color);
	void(*drawpoint)(u16 x, u16 y);
	void(*drawpointBig)(u16 x, u16 y);
	void(*drawline)(u16 x1, u16 y1, u16 x2, u16 y2);
	void(*drawCircle)(u16 x0, u16 y0, u8 r);
	void(*drawRectangle)(u16 x1, u16 y1, u16 x2, u16 y2);
	void(*fill)(u16 xsta, u16 ysta, u16 xend, u16 yend, u16 color);
	void(*shownum)(u16 x, u16 y, u32 num, u8 len);
	void(*showstr)(u16 x, u16 y, const char *p);
	void(*clear)(u16 Color);
}lcdtype;


typedef struct eyetpye_
{
	u16 width;
	u16 height;
	u8 getok;

	u8** img;
	void(*init)();
	void(*led)(u8 on);
	void(*size)(u16 width, u16 height);
	void(*get)();
	void(*go)();

}eyetype;


typedef struct keytpye_
{
	void(*init)();
	u8(*upkey)();
	u8(*downkey)();
	u8(*leftkey)();
	u8(*rightkey)();
	u8(*scan)(u8 contin);

}keytype;

typedef struct memtpye_
{
	void(*init)();
	void*(*CCMmalloc)(u32 size);
	void*(*SRAM0malloc)(u32 size);
	void*(*SRAM1malloc)(u32 size);
	void(*CCMfree)(void *ptr);
	void(*SRAM0free)(void *ptr);
	void(*SRAM1free)(void *ptr);
}memtype;


typedef struct usartpye_
{
	void(*init)();
	int(*print)(const char*, ...);
	u8* (*read)();

}uarttype;

typedef struct graphtpye_
{
	lcdtype *lcd;
	keytype *key;
	memtype *mem;
	eyetype *eye;
	uarttype *usart0;
}graphtpye;

extern lcdtype mylcd;

extern keytype mykey;
extern memtype mem;
extern eyetype myeye;
extern uarttype uart0;
extern graphtpye graph;

void graph_init(void);
void graph_ok(void);
void point_color(u16 color);
void back_color(u16 color);

void meminit(void);
void *CCMmalloc(u32 size);
void *SRAMmalloc(u32 size);
void CCMfree(void *ptr);
void SRAMfree(void *ptr);

u8 upkey(void);
u8 downkey(void);
u8 leftkey(void);
u8 rightkey(void);

#endif

