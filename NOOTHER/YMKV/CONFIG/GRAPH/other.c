#include "Puse.h"
#include "lcd.h"
#include "graph.h"
#include "other.h"

u16 LCD_W = 640, LCD_H = 480;
u8 qrencode_mem = 0;
u8 zbar_mem = 1;

ImgType CAM;
/***********************************

功能：    显示

************************************/
//				RGB565格式图像在LCD上显示
void YMKV_show_pic(u16 x, u16 y, u16 Width, u16 Height, u16 *pic)
{
	u16 i, j;
	u16 color565;

	for (i = 0; i < Height; i++)
	{
		for (j = 0; j < Width; j++)
		{
			color565 = *(pic + i * Width + j);
			LCD_point_u16(x + j, y + i, color565);
		}
	}
}
void YMKV_show_pic2(u16 x, u16 y, ImgType showImg)
{
	if (showImg == Null)return;
	YMKV_show_pic(x, y, showImg->width, showImg->height, (u16*)showImg->data);
}
void YMKV_show_gray(u16 x, u16 y, u16 Width, u16 Height, u8 *pic)
{
	u16 i, j;
	u8 gray;

	for (i = 0; i < Height; i++)
	{
		for (j = 0; j < Width; j++)
		{
			gray = *(pic + i * Width + j);
			LCD_point_u8(x + j, y + i, gray);
		}
	}
}
void YMKV_show_gray2(u16 x, u16 y, ImgType showImg)
{
	if (showImg == Null)return;
	YMKV_show_gray(x, y, showImg->width, showImg->height, showImg->data);
}
void YMKV_show(u16 x, u16 y, ImgType showImg)
{
	if (showImg == Null)return;
	switch (showImg->sizet)
	{
	case imgGray: YMKV_show_gray(x, y, showImg->width, showImg->height, showImg->data); break;
	case imgRGB:  YMKV_show_pic(x, y, showImg->width, showImg->height, (u16*)showImg->data); break;
	}
}
void draw_blobs(u16 startx, u16 starty, BlobType pthis)
{
	//char str[15];
	u16 pts = POINT_COLOR;

	if (pthis == Null)return;
	while (pthis != NULL)
	{
		graph.lcd->point_color(BLUE);
		//画出中心坐标区域
		graph.lcd->fill(startx + (pthis->wp + (pthis->width / 2)) - 2, starty + (pthis->hp + (pthis->height / 2)) - 2, startx + (pthis->wp + (pthis->width / 2)) + 2, starty + (pthis->hp + (pthis->height / 2)) + 2, RED);
		//				graph.lcd->shownum(startx+pthis->wp,starty+pthis->hp,(pthis->wp+(pthis->width/2)),3);//显示坐标w
		//				graph.lcd->shownum(startx+pthis->wp,starty+pthis->hp+16,(pthis->hp+(pthis->height/2)),3);//显示坐标h
		graph.lcd->shownum(startx + pthis->wp, starty + pthis->hp, pthis->value, 1);
		graph.lcd->drawRectangle(startx + pthis->wp, starty + pthis->hp, startx + pthis->wp + pthis->width, starty + pthis->hp + pthis->height);
		pthis = pthis->lastlist;
	}
	graph.lcd->point_color(pts);
}
void IMG_Init()
{
	CAM = Creat.data2img((u8*)graph.eye->img, graph.eye->width, graph.eye->height, imgRGB);//数据封装为彩色图
}
void Temp_Init(ImgType temp)
{
	u16 i, j;
	static u8 nofirst = 0;

	if (nofirst)return;

	nofirst = 1;
	for (i = 0; i < temp->height; i++)
		for (j = 0; j < temp->width; j++)
			temp->data[i*temp->width + j] = ~temp->data[i*temp->width + j];
}
void qshow(int startx, int starty, int width)
{
	int i, endx, endy;
	float deta = width / 255.0f;
	u16 ptc = POINT_COLOR;
	graph.lcd->point_color(RED);
	endx = startx + width;
	endy = starty - width;
	graph.lcd->fill(startx, starty - width, startx + width, starty, WHITE);//清空区域
	graph.lcd->showstr(startx, starty, "0");
	graph.lcd->drawRectangle(startx, starty, endx, endy); graph.lcd->point_color(BLUE);
	graph.lcd->point_color(BLUE);
	for (i = 0; i < width; i++)
	{
		graph.lcd->drawpoint(startx + (int)(((float)i)), (starty - (int)(((float)pix2pix[i * 255 / width] * deta))));
	}
	graph.lcd->point_color(ptc);
}

void Histshow(ImgType gray, int startx, int starty, int width)
{
	int i, endx, endy, maxH, j;
	float deta = width / 255.0f, detakh;
	u16 ptc = POINT_COLOR;
	u8 showHis[256];

	graph.lcd->point_color(RED);
	endx = startx + width;
	endy = starty - width;
	graph.lcd->fill(startx, starty - width, startx + width, starty, WHITE);//清空区域
	graph.lcd->showstr(startx, starty, "0");
	graph.lcd->drawRectangle(startx, starty, endx, endy);
	maxH = 0;
	YMKV.KV->His_get(gray, statistic);
	for (i = 0; i < 256; i++)
	{
		showHis[i] = 0;
		if (maxH < statistic[i])
			maxH = statistic[i];
	}
	detakh = 225.0f / maxH;  j = 0;
	for (i = 0; i < width; i++)
	{
		do {
			showHis[i] += (statistic[j] * detakh);
			j++;
		} while (j < (i * 255) / width);
	}
	graph.lcd->point_color(BLUE);
	for (i = 0; i < width; i++)
	{
		graph.lcd->drawline(startx + i, starty, startx + i, starty - (showHis[i] * deta));
	}
	graph.lcd->point_color(ptc);
}
void drawline_rou_theta(u16 startx, u16 starty, ImgType myimg, SVectordType rou_theta)
{
	u16 rou_startx = 200, rou_starty = 0;
	u16 num = (rou_theta->len_size) / 2;
	u16 i, j, Width, Height, ik, jk, fii[4], myxy[4];
	short hfrou;
	float sintheta, costheta, fk[4];
	u16 ptc = POINT_COLOR;

	Width = myimg->width;
	Height = myimg->height;
	hfrou = Width + Height;
#if (HfLT==90)
	hfrou = 0;
#endif
	//		printf("num=%d \n",num);

	graph.lcd->point_color(RED);
	for (i = 0; i < num; i++)
	{
		ik = rou_theta->data[2 * i + 1];//ik <=> theta
		jk = rou_theta->data[2 * i];
		//printf("%d %d \n",ik,jk);
		sintheta = sin(ik*pai / 180);
		costheta = cos(ik*pai / 180);
		//   graph.lcd->drawline(0,(u16)((jk-hfrou)/sintheta),(u16)((jk -hfrou)/costheta),0);
		graph.lcd->point_color(GREEN);
		graph.lcd->drawpoint(jk - 1, ik + rou_startx - 1);
		graph.lcd->drawpoint(jk - 1, ik + rou_startx);
		graph.lcd->drawpoint(jk - 1, ik + rou_startx + 1);
		graph.lcd->drawpoint(jk, ik + rou_startx - 1);
		graph.lcd->drawpoint(jk, ik + rou_startx);
		graph.lcd->drawpoint(jk, ik + rou_startx + 1);
		graph.lcd->drawpoint(jk + 1, ik + rou_startx - 1);
		graph.lcd->drawpoint(jk + 1, ik + rou_startx);
		graph.lcd->drawpoint(jk + 1, ik + rou_startx + 1);

		fii[0] = 0;
		fii[1] = Width;
		fii[2] = 0;
		fii[3] = Height;

		fk[0] = (jk - hfrou) / sintheta;
		fk[1] = (jk - hfrou) / sintheta - Width * costheta / sintheta;
		fk[2] = (jk - hfrou) / costheta;
		fk[3] = (jk - hfrou) / costheta - Height * sintheta / costheta;
		ik = 0; jk = 0;
		for (j = 0; j < 4; j++)
		{
			if (fk[j] >= 0)
			{
				if ((j < 2 && fk[j] < Height + 2) || (j > 1 && fk[j] < Width + 2))
				{
					if (ik == 0)
						ik = j;
					else
						jk = j;
				}

			}
			if (jk != 0)
				break;
		}
		if (ik > 1) {
			myxy[1] = fii[ik];
			myxy[0] = fk[ik];
		}
		else {
			myxy[0] = fii[ik];
			myxy[1] = fk[ik];
		}

		if (jk > 1) {
			myxy[3] = fii[jk];
			myxy[2] = fk[jk];
		}
		else {
			myxy[2] = fii[jk];
			myxy[3] = fk[jk];
		}
		graph.lcd->point_color(RED);
		graph.lcd->drawline(startx + myxy[0], starty + myxy[1], startx + myxy[2], starty + myxy[3]);
	}
	graph.lcd->point_color(ptc);
}
void drawline_hough(u16 startx, u16 starty, LinesType mylines)
{
	if (mylines == Null) return;
	graph.lcd->point_color(RED);
	while (mylines->lastline != Null)
	{
		graph.lcd->drawline(startx + mylines->xy[0], starty + mylines->xy[1], startx + mylines->xy[2], starty + mylines->xy[3]);
		mylines = mylines->lastline;
	}
	graph.lcd->drawline(startx + mylines->xy[0], starty + mylines->xy[1], startx + mylines->xy[2], starty + mylines->xy[3]);
}
void showfeature(u16 startx, u16 starty, FeatureList myfeature)
{
	u16 i, j;
	u16 pst = POINT_COLOR;
	graph.lcd->point_color(RED);
	while (myfeature != Null)
	{
		if (myfeature->thisfeature != Null)
		{
			j = myfeature->thisfeature->xyc[0];
			i = myfeature->thisfeature->xyc[1];

			graph.lcd->drawpointBig(j + startx, i + starty);
		}
		myfeature = myfeature->lastlist;
	}
	graph.lcd->point_color(pst);
}
void qrlist_show(u16 startx, u16 starty, QRListType pthis)
{
	u16 pst = POINT_COLOR;
	graph.lcd->point_color(RED);
	while (pthis)
	{
		graph.lcd->drawRectangle(startx + pthis->wp, starty + pthis->hp, startx + pthis->width, starty + pthis->height);
		graph.lcd->showstr(startx + pthis->wp, starty + pthis->hp, pthis->type);
		graph.lcd->showstr(startx + pthis->wp, starty + pthis->hp + 16, pthis->data);
		pthis = pthis->free(pthis);
	}
	graph.lcd->point_color(pst);
}