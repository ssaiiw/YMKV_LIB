#include"graph.h"
#include"camera.h"
#include"YMKV.h"
#include"UWindows.h"
#include"lcd.h"
#include"stdio.h" 
#include"pic.h" 
//********************************************************************************	 
//本例程只供学习使用，未经许可，不得用于其它任何用途
//YMKV开发板
//底层接口配置代码 
//项目：妖米视觉
//创建日期:2019/11/5
//版本：V1.0
//版权所有，盗版必究。
//沈阳妖米科技责任有限公司
//********************************************************************************

// struct init 
lcdtype mylcd;
graphtpye graph;
keytype mykey;
memtype mymem;
eyetype myeye;
uarttype usart0;


//key 初始化
void KEY_Init()
{

}
u8 upkey() {
	return 'w';
}
u8 downkey() {
	return 's';
}
u8 leftkey() {
	return 'a';
}
u8 rightkey() {
	return 'd';
}
u8 key_scan(u8 a)
{
	u8 da = getch();
	if (da == 'w')return Upm;
	if (da == 's')return Downm;
	if (da == 'a')return Leftm;
	if (da == 'd')return Rightm;
	else return 0;
}


// uart 初始化
void usart0_init()
{

}

u8* usart0_read()
{
	//	if((USART_RX_FLG&0x8000)==0)
	//	{
	//		return USART_RX_BUF;
	//	}
	//	else
	//		return NULL;
}
// malloc 初始化
void meminit()
{

}
//申请内存
void *CCMmalloc(u32 size)
{
	return malloc(size);
}
void *SRAM0malloc(u32 size)
{
	return malloc(size);
}
void *SRAMmalloc(u32 size)
{
	return malloc(size);
}
//释放内存
void CCMfree(void *ptr)
{
	free(ptr);
}
void SRAM0free(void *ptr)
{
	free(ptr);
}
void SRAMfree(void *ptr)
{
	free(ptr);
}

//eye init 
void eye_init()
{
	CameraInit();//默认QQVGA
	graph.eye->img = (u8**)pic_buf;
	graph.eye->height = pic_h;
	graph.eye->width = pic_w;
	graph.eye->getok = 1;
}

void eye_led(u8 on)
{

}

void eye_size(u16 width, u16 height)
{
	myeye.height = height;
	myeye.width = width;
}

void pic_get()
{
	if (graph.eye->getok == 1)
	{
		YMKV.imgok = 1;
	}
}

void pic_go()
{
	YMKV.imgok = 0;
	continue_pic();
}
/***********************

graph init

***********************/
void graph_init()
{
	//lcd init
	mylcd.init = LCD_Init;
	mylcd.point_color = point_color;
	mylcd.back_color = back_color;
	mylcd.drawpoint = LCD_DrawPoint;
	mylcd.drawpointBig = LCD_DrawPoint_big;
	mylcd.drawline = LCD_DrawLine;
	mylcd.drawCircle = Draw_Circle;
	mylcd.drawRectangle = LCD_DrawRectangle;
	mylcd.fill = LCD_Fill;
	mylcd.shownum = LCD_ShowNum;
	mylcd.showstr = LCD_ShowString;
	mylcd.clear = LCD_Clear;


	//key init 
	mykey.init = KEY_Init;
	mykey.upkey = upkey;
	mykey.downkey = downkey;
	mykey.leftkey = leftkey;
	mykey.rightkey = rightkey;
	mykey.scan = key_scan;


	//eye init
	myeye.init = eye_init;
	myeye.led = eye_led;
	myeye.get = pic_get;
	myeye.go = pic_go;

	//memo init
	mymem.init = meminit;
	mymem.CCMmalloc = CCMmalloc;
	mymem.CCMfree = CCMfree;
	mymem.SRAM0malloc = SRAM0malloc;
	mymem.SRAM0free = SRAM0free;
	mymem.SRAM1malloc = SRAMmalloc;
	mymem.SRAM1free = SRAMfree;



	//usart0 init
	usart0.init = usart0_init;
	usart0.print = printf;
	usart0.read = usart0_read;

	// graph init
	graph.lcd = &mylcd;
	graph.key = &mykey;
	graph.usart0 = &usart0;
	graph.eye = &myeye;
	graph.mem = &mymem;

	// 底层 init
	graph.usart0->init();//初始化串口
	graph.lcd->init();//初始化LCD
	graph.key->init();//初始化按键
	graph.mem->init();//初始化内存配置
	graph.eye->init();//初始化摄像头配置

	graph_ok();//初始化成功
}

void graph_ok()
{

	graph.lcd->clear(WHITE);
	graph.lcd->point_color(BLACK);
	graph.lcd->back_color(WHITE);
	graph.lcd->showstr(30, 40, "YAOMI K-V\n");
	graph.lcd->showstr(30, 70, "MEACHINE VERSION\n");
	graph.lcd->showstr(30, 100, "graph ok\n");
	WINDOW->update(WINDOW);//更新显示窗口内容
	Sleep(1000);
	graph.lcd->clear(WHITE);
}


