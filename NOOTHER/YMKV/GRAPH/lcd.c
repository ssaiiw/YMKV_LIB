#include "lcd.h"
#include "stdlib.h"
#include "font.h"  	 
#include "UWindows.h"

u16 BACK_COLOR = WHITE, POINT_COLOR = BLACK;   //背景色，画笔色
u16 MyWIN_W, MyWIN_H;
void LCD_Init()
{	// 获取一个可供画图的DC，我这里就直接用桌面算了
	//HDC hdc = GetWindowDC( GetDesktopWindow() );
	hWnd = GetConsoleWindow();
	hDC = GetWindowDC(hWnd);
	WINDOW = WinCreat.NewWin(LCD_W, LCD_H);//创建绘图显示区
	WINDOW->setXY(WINDOW, 200, 50);//改变窗口起始位置
	WINDOW->update(WINDOW);//绘制显示窗口

}

//清屏函数
//Color:要清屏的填充色
void LCD_Clear(u16 Color)
{
	u16 i, j;
	int pos = 0;
	u16 ptc = POINT_COLOR;
	POINT_COLOR = Color;
	MyWIN_W = WINDOW->width;
	MyWIN_H = WINDOW->height;

	for (i = 0; i < MyWIN_W; i++)
	{
		for (j = 0; j < MyWIN_H; j++)
		{
			LCD_DrawPoint(i, j);
		}
	}
	POINT_COLOR = ptc;
}


//在指定位置显示一个汉字(32*32大小)
//dcolor为内容颜色，gbcolor为背静颜色
void showhanzi(u16 x, u16 y, unsigned char index)
{
	unsigned char i, j;
	unsigned char *temp = hanzi;
	u16 ptc = POINT_COLOR;
	//Address_set(x,y,x+31,y+31); //设置区域      
	temp += index * 128;
	for (j = 0; j < 128; j++)
	{
		for (i = 0; i < 8; i++)
		{
			u16 xm = j / 4;
			u16 ym = (j%xm) + i;
			if ((*temp&(1 << i)) != 0)
			{
				//LCD_WR_DATA(POINT_COLOR);
				POINT_COLOR = ptc;
				LCD_DrawPoint(xm + x, ym + y);
			}
			else
			{
				//LCD_WR_DATA(BACK_COLOR);
				POINT_COLOR = BACK_COLOR;
				LCD_DrawPoint(xm + x, ym + y);
			}
		}
		temp++;
	}
	POINT_COLOR = ptc;
}

//画点
//POINT_COLOR:此点的颜色
void LCD_DrawPoint(u16 x, u16 y)
{
	MyWIN_W = WINDOW->width;
	MyWIN_H = WINDOW->height;

	if (x > MyWIN_W || y > MyWIN_H)
		return;
	int pos = 4 * (y * MyWIN_W + x);
	u8 R, G, B;
	YMKV_rgb565toRGB3(POINT_COLOR, &R, &G, &B);
	WINDOW->imdata[pos++] = B;//B
	WINDOW->imdata[pos++] = G;//G
	WINDOW->imdata[pos++] = R;//R
	WINDOW->imdata[pos++] = 0;//alpha	 
	//LCD_WR_DATA(POINT_COLOR);
}
//画一个大点
//POINT_COLOR:此点的颜色
void LCD_DrawPoint_big(u16 x, u16 y)
{
	MyWIN_W = WINDOW->width;
	MyWIN_H = WINDOW->height;

	if (x > MyWIN_W || y > MyWIN_H)
		return;
	LCD_Fill(x - 1, y - 1, x + 1, y + 1, POINT_COLOR);
}
//在指定区域内填充指定颜色
//区域大小:
//  (xend-xsta)*(yend-ysta)
void LCD_Fill(u16 xsta, u16 ysta, u16 xend, u16 yend, u16 color)
{
	u16 i, j;
	u16 ptc = POINT_COLOR;
	POINT_COLOR = color;
	for (i = ysta; i <= yend; i++)
	{
		for (j = xsta; j <= xend; j++)
		{
			LCD_DrawPoint(j, i);
		}
	}
	POINT_COLOR = ptc;
}
//画线
//x1,y1:起点坐标
//x2,y2:终点坐标  
void LCD_DrawLine(u16 x1, u16 y1, u16 x2, u16 y2)
{
	u16 t;
	int xerr = 0, yerr = 0, delta_x, delta_y, distance;
	int incx, incy, uRow, uCol;

	delta_x = x2 - x1; //计算坐标增量 
	delta_y = y2 - y1;
	uRow = x1;
	uCol = y1;
	if (delta_x > 0)incx = 1; //设置单步方向 
	else if (delta_x == 0)incx = 0;//垂直线 
	else { incx = -1; delta_x = -delta_x; }
	if (delta_y > 0)incy = 1;
	else if (delta_y == 0)incy = 0;//水平线 
	else { incy = -1; delta_y = -delta_y; }
	if (delta_x > delta_y)distance = delta_x; //选取基本增量坐标轴 
	else distance = delta_y;
	for (t = 0; t <= distance + 1; t++)//画线输出 
	{
		LCD_DrawPoint(uRow, uCol);//画点 
		xerr += delta_x;
		yerr += delta_y;
		if (xerr > distance)
		{
			xerr -= distance;
			uRow += incx;
		}
		if (yerr > distance)
		{
			yerr -= distance;
			uCol += incy;
		}
	}
}
//画矩形
void LCD_DrawRectangle(u16 x1, u16 y1, u16 x2, u16 y2)
{
	LCD_DrawLine(x1, y1, x2, y1);
	LCD_DrawLine(x1, y1, x1, y2);
	LCD_DrawLine(x1, y2, x2, y2);
	LCD_DrawLine(x2, y1, x2, y2);
}
//在指定位置画一个指定大小的圆
//(x,y):中心点
//r    :半径
void Draw_Circle(u16 x0, u16 y0, u8 r)
{
	int a, b;
	int di;
	a = 0; b = r;
	di = 3 - (r << 1);             //判断下个点位置的标志
	while (a <= b)
	{
		LCD_DrawPoint(x0 - b, y0 - a);             //3           
		LCD_DrawPoint(x0 + b, y0 - a);             //0           
		LCD_DrawPoint(x0 - a, y0 + b);             //1       
		LCD_DrawPoint(x0 - b, y0 - a);             //7           
		LCD_DrawPoint(x0 - a, y0 - b);             //2             
		LCD_DrawPoint(x0 + b, y0 + a);             //4               
		LCD_DrawPoint(x0 + a, y0 - b);             //5
		LCD_DrawPoint(x0 + a, y0 + b);             //6 
		LCD_DrawPoint(x0 - b, y0 + a);
		a++;
		//使用Bresenham算法画圆     
		if (di < 0)di += 4 * a + 6;
		else
		{
			di += 10 + 4 * (a - b);
			b--;
		}
		LCD_DrawPoint(x0 + a, y0 + b);
	}
}
//在指定位置显示一个字符
//num:要显示的字符:" "--->"~"
//mode:叠加方式(1)还是非叠加方式(0)
//在指定位置显示一个字符
//num:要显示的字符:" "--->"~"
//mode:叠加方式(1)还是非叠加方式(0)
void LCD_ShowChar(u16 x, u16 y, u8 num, u8 mode)
{
	u8 temp;
	u8 pos, t;
	u16 x0 = x;
	u16 colortemp = POINT_COLOR;
	MyWIN_W = WINDOW->width;
	MyWIN_H = WINDOW->height;

	if (x > MyWIN_W - 16 || y > MyWIN_H - 16)
		return;
	//设置窗口		   
	num = num - ' ';//得到偏移后的值
	//Address_set(x,y,x+8-1,y+16-1);      //设置光标位置 
	if (!mode) //非叠加方式
	{
		for (pos = 0; pos < 16; pos++)
		{
			temp = asc2_1608[(u16)num * 16 + pos];		 //调用1608字体
			for (t = 0; t < 8; t++)
			{
				if (temp & 0x01)POINT_COLOR = colortemp;
				else POINT_COLOR = BACK_COLOR;
				/*u16 xm = pos / 2;
				u16 ym = (pos % xm) + t;*/
				LCD_DrawPoint(t + x, pos + y);
				//LCD_WR_DATA(POINT_COLOR);	
				temp >>= 1;
				//x++;
			}
			//x=x0;
			//y++;
		}
	}
	else//叠加方式
	{
		for (pos = 0; pos < 16; pos++)
		{
			temp = asc2_1608[(u16)num * 16 + pos];		 //调用1608字体
			for (t = 0; t < 8; t++)
			{
				if (temp & 0x01)
					LCD_DrawPoint(x + t, y + pos);//画一个点     
				temp >>= 1;
			}
		}
	}
	POINT_COLOR = colortemp;
}
//m^n函数
u32 mypow(u8 m, u8 n)
{
	u32 result = 1;
	while (n--)result *= m;
	return result;
}
//显示2个数字
//x,y :起点坐标	 
//len :数字的位数
//color:颜色
//num:数值(0~4294967295);	
void LCD_ShowNum(u16 x, u16 y, u32 num, u8 len)
{
	u8 t, temp;
	u8 enshow = 0;
	num = (u16)num;
	for (t = 0; t < len; t++)
	{
		temp = (num / mypow(10, len - t - 1)) % 10;
		if (enshow == 0 && t < (len - 1))
		{
			if (temp == 0)
			{
				LCD_ShowChar(x + 8 * t, y, ' ', 0);
				continue;
			}
			else enshow = 1;

		}
		LCD_ShowChar(x + 8 * t, y, temp + 48, 0);
	}
}
//显示2个数字
//x,y:起点坐标
//num:数值(0~99);	 
void LCD_Show2Num(u16 x, u16 y, u16 num, u8 len)
{
	u8 t, temp;
	for (t = 0; t < len; t++)
	{
		temp = (num / mypow(10, len - t - 1)) % 10;
		LCD_ShowChar(x + 8 * t, y, temp + '0', 0);
	}
}
//显示字符串
//x,y:起点坐标  
//*p:字符串起始地址
//用16字体
void LCD_ShowString(u16 x, u16 y, const char *p)
{
	while (*p != '\0')
	{
		MyWIN_W = WINDOW->width;
		MyWIN_H = WINDOW->height;

		if (x > MyWIN_W - 16) { x = 0; y += 16; }
		if (y > MyWIN_H - 16) { y = x = 0; LCD_Clear(RED); }
		LCD_ShowChar(x, y, *p, 0);
		x += 8;
		p++;
		if (*p == '\n')
		{
			x += 8;
			p++;
		}
	}
}




void LCD_point_u16(u16 x, u16 y, u16 color565)
{
	u8 R, G, B;
	MyWIN_W = WINDOW->width;
	int pos = 4 * (y * MyWIN_W + x);
	YMKV_rgb565toRGB3(color565, &R, &G, &B);
	WINDOW->imdata[pos++] = B;//B
	WINDOW->imdata[pos++] = G;//G
	WINDOW->imdata[pos++] = R;//R
	WINDOW->imdata[pos++] = 0;//alpha	 
}
void LCD_point_u8(u16 x, u16 y, u8 gray)
{
	MyWIN_W = WINDOW->width;
	int pos = 4 * (y * MyWIN_W + x);

	WINDOW->imdata[pos++] = gray;//B
	WINDOW->imdata[pos++] = gray;//G
	WINDOW->imdata[pos++] = gray;//R
	WINDOW->imdata[pos++] = 0;//alpha	
}


//void lineDDA(int x0, int xEnd, int y0, int yEnd)
//{
//	HWND   hWnd = GetConsoleWindow();   //获得控制台窗口的句柄 
//	HDC   hDC = GetWindowDC(hWnd);//通过窗口句柄得到该窗口的设备场境句柄 
//	int dx, dy, steps, k;
//	float xIncrease, yIncrease, x = x0, y = y0;
//	dx = xEnd - x0;
//	dy = yEnd - y0;
//	if (fabs((float)dx) > fabs((float)dy))
//		steps = dx;
//	else
//		steps = dy;
//	xIncrease = (float)dx / (float)steps;
//	yIncrease = (float)dy / (float)steps;
//	SetPixel(hDC, round(x), round(y), RGB(255, 0, 0));
//	for (k = 0; k < steps; ++k)
//	{
//		x += xIncrease;
//		y += yIncrease;
//		SetPixel(hDC, round(x), round(y), RGB(255, 0, 0));
//	}
//}
